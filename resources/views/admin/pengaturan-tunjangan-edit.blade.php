@extends('layouts.mainadmin')

@section('dashboard')
<div class="container">
    <div class="row">
        <div class="col">
            <a href="#">kembali</a>
            <form method="post" action="/pengaturan-tunjangan">
                @csrf
                @method('put')
                <hr>
                <div class="mb-3 col-4">
                    <label for="jenis_tunjangan" class="form-label">Jenis Tunjangan</label>
                    <input type="text" class="form-control" id="jenis_tunjangan" name="jenis_tunjangan" value="{{ $data['jenis_tunjangan'] }}">
                </div>
                <div class="mb-3 col-4">
                    <label for="nominal" class="form-label">Nominal</label>
                    <input type="number" class="form-control" id="nominal" name="nominal" value="{{ $data['nominal'] }}">
                </div>
                <button type="submit" class="btn btn-warning">Edit</button>
            </form>
        </div>
    </div>
</div>
@endsection
