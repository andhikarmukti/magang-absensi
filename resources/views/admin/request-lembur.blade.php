@extends('layouts.mainadmin')

@section('dashboard')
<div class="container">
    <div class="row">
        <div class="col-4">
            <h3 class="mt-2 mb-4 text-center">Pengajuan Lembur</h3>

            @if (session()->has('berhasilSubmit'))
            <div class="alert alert-success alert-dismissible fade show mt-4" role="alert">
                {{ session('berhasilSubmit') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
            @endif

            <form action="/request-lembur" method="post">
                @csrf
                <input type="hidden" class="form-control" id="user_id" name="user_id" readonly value="{{ auth()->user()->id }}">
                <div class="mb-3">
                    <label for="name" class="form-label">Nama</label>
                    <input type="text" class="form-control" id="name" name="name" readonly value="{{ auth()->user()->name }}">
                </div>
                <div class="mb-3">
                    <label for="tanggal_lembur" class="form-label">Tanggal Lembur</label>
                    <input type="date" class="form-control" id="tanggal_lembur" name="tanggal_lembur">
                </div>
                <div class="mb-3">
                    <label for="dari_jam_lembur" class="form-label">Dari Jam</label>
                    <input type="time" class="form-control" id="dari_jam_lembur" name="dari_jam_lembur">
                </div>
                <div class="mb-3">
                    <label for="hingga_jam_lembur" class="form-label">Hingga Jam</label>
                    <input type="time" class="form-control" id="hingga_jam_lembur" name="hingga_jam_lembur">
                </div>
                <div class="mb-3">
                    <label for="keterangan" class="form-label">Keterangan</label>
                    <input type="text" class="form-control" id="keterangan" name="keterangan">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection