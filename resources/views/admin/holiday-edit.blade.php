@extends('layouts.mainadmin')

@section('dashboard')
<div class="container">
    <div class="row">
        <div class="col mt-5">

            @if(session()->has('berhasilUpdate'))
            <div class="alert alert-success alert-dismissible fade show mt-4 col-6" role="alert">
                {{ session('berhasilUpdate') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
            @endif

            <form action="/holiday" method="post">
                @csrf
                <div class="mb-3 row">
                    <label for="tanggal" class="col-sm-2 col-form-label">Tanggal :</label>
                    <div class="col-sm-5">
                        <label for="floatingInputInvalid">{{ $holiday[0]['holiday_date'] }}</label>
                        <input type="date" class="form-control" id="tanggal" name="tanggal">
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="perayaan" class="col-sm-2 col-form-label">Perayaan :</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="perayaan" name="perayaan" value="{{ $holiday[0]['holiday_name'] }}" readonly>
                    </div>
                </div>
                <div class="mb-3 row">
                    <label for="perayaan" class="col-sm-2 col-form-label">Libur Nasional?</label>
                    <div class="col-sm-5">
                        <input type="text" class="form-control" id="perayaan" name="perayaan" value="{{ $holiday[0]['is_national_holiday'] == true ? 'Ya' : '' }}" readonly>
                    </div>
                </div>
                <input type="hidden" name="id" value="{{ $holiday[0]['id'] }}">
                <button class="btn btn-primary mb-5" type="submit">Ubah</button>
            </form>
            <a href="/holiday"><span class="badge bg-primary">Kembali</span></a>
        </div>
    </div>
</div>
@endsection