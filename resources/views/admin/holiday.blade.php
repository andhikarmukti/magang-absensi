@extends('layouts.mainadmin')

@section('dashboard')
    <div class="container">
        <div class="row">
            <div class="col">
                <h1>Daftar Libur 2021</h1>
                <table class="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col">Tanggal</th>
                        <th scope="col">Perayaan</th>
                        <th scope="col">Hari Libur Nasional?</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($holiday as $h)
                        <tr>
                          <th scope="row">{{ $h->holiday_date }}</th>
                          <td>{{ $h->holiday_name }}</td>
                          <td>@if($h->is_national_holiday == true)
                            {{ 'Ya' }}
                            @endif</td>
                            <td>
                                <a href="/holiday/edit/{{ $h->id }}" class="badge bg-warning"><span>edit</span></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
@endsection