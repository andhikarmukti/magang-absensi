@extends('layouts.mainadmin')

@section('dashboard')
<div class="container">
    <div class="row">
        <div class="col">
            <a href="/list-request-cuti" class="badge bg-primary">kembali</a>
            <table class="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">Nama</th>
                    <th scope="col">Dari Tanggal</th>
                    <th scope="col">Hingga Tanggal</th>
                    <th scope="col">Jenis Cuti</th>
                    <th scope="col">Keterangan</th>
                    <th scope="col">Approval</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($data as $d)
                    <tr>
                        <th scope="row">{{ $d->name }}</th>
                        <td>{{ $d->dari_tanggal }}</td>
                        <td>{{ $d->hingga_tanggal }}</td>
                        <td>{{ $d->jenis_cuti }}</td>
                        <td>{{ $d->keterangan }}</td>
                        <td>{{ $d->approval }}</td>
                        <td>
                            <a href="/accept-request-cuti/{{ $d->id }}" class="badge bg-warning" onclick="return confirm('anda yakin?')">edit</a>
                        </td>
                      </tr>
                    @endforeach
                </tbody>
              </table>
        </div>
    </div>
</div>
@endsection
