@extends('layouts.mainadmin')

@section('dashboard')
<div class="container">
    <div class="row mt-5">
        <div class="col">
            <table class="table">
                    <thead>
                        <tr>
                          <th scope="col">ID</th>
                          <th scope="col">Nama</th>
                          <th scope="col">Check In</th>
                          <th scope="col">Check Out</th>
                          <th scope="col">Total Jam Kerja</th>
                          <th scope="col">Check In Image</th>
                          <th scope="col">Check Out Image</th>
                        </tr>
                      </thead>
                      <tbody>
                          @php
                              $i = 0;
                          @endphp
                          @foreach ($data as $d)
                          @php
                              $i++;
                          @endphp
                          <tr>
                            <th scope="row">{{ $d->id }}</th>
                            <td>{{ $d->name }}</td>
                            <td>{{ $d->check_in }}</td>
                            <td>{{ $d->check_out }}</td>
                            <td>{{ $d->selisih_waktu }}</td>
                            <td><a href="/storage/img/{{ $d->check_in_image }}" target="_blank">open image</a></td>
                            <td><a href="/storage/img/{{ $d->check_out_image }}" target="_blank">open image</a></td>
                          </tr>
                          @endforeach
                      </tbody>
                </table>
                <div class= "d-flex justify-content-center">
                    {{ $data->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection

