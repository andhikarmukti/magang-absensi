@extends('layouts.mainadmin')

@section('dashboard')
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="text-center">List Absensi</h1>

                @if (session()->has('bulanTidakAda'))
                <div class="alert alert-danger alert-dismissible fade show mt-4" role="alert">
                    {{ session('bulanTidakAda') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                  </div>
                @endif

                <form action="/history" method="get">
                    <div class="input-group mb-3 col-5">
                        <input type="text" class="form-control" placeholder="Search.." autofocus name="keyword" value="{{ request('keyword') }}">
                        <button class="btn btn-primary" type="submit">Search</button>
                        <select class="form-select" name="bulan" id="bulan">
                            <option value="01" {{ !request('bulan') ? 'selected disabled' : 'disabled'}}>Semua Data</option>
                            <option value="01" {{ request('bulan') == '01' ? 'selected' : ""}}>Januari</option>
                            <option value="02" {{ request('bulan') == '02' ? 'selected' : ""}}>Februari</option>
                            <option value="03" {{ request('bulan') == '03' ? 'selected' : ""}}>Maret</option>
                            <option value="04" {{ request('bulan') == '04' ? 'selected' : ""}}>April</option>
                            <option value="05" {{ request('bulan') == '05' ? 'selected' : ""}}>Mei</option>
                            <option value="06" {{ request('bulan') == '06' ? 'selected' : ""}}>Juni</option>
                            <option value="07" {{ request('bulan') == '07' ? 'selected' : ""}}>Juli</option>
                            <option value="08" {{ request('bulan') == '08' ? 'selected' : ""}}>Agustus</option>
                            <option value="09" {{ request('bulan') == '09' ? 'selected' : ""}}>September</option>
                            <option value="10" {{ request('bulan') == '10' ? 'selected' : ""}}>Oktober</option>
                            <option value="11" {{ request('bulan') == '11' ? 'selected' : ""}}>November</option>
                            <option value="12" {{ request('bulan') == '12' ? 'selected' : ""}}>Desember</option>
                        </select>
                    </div>
                    <a class="mb-3 badge bg-danger" href="/history">Clear</a>
                </form>

                    <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">Nama</th>
                                    <th scope="col">Shift</th>
                                    <th scope="col">Check In</th>
                                    <th scope="col">Check Out</th>
                                    <th scope="col">Total Jam Kerja</th>
                                    <th scope="col">keterangan</th>
                                <th scope="col">Check In Image</th>
                                <th scope="col">Check Out Image</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($data as $d)
                                <tr>
                                    <td>{{ $d->name }}</td>
                                    <td>{{ $d->shift }}</td>
                                    <td>{{ $d->check_in }}</td>
                                    <td>{{ $d->check_out }}</td>
                                    <td>{{ $d->selisih_waktu }}</td>
                                    <th scope="row" style="{{ $d->keterangan == 'telat' ? 'color: red' : 'color: green' }}">{{ $d->keterangan }}</th>
                                    <td><a href="/storage/img/{{ $d->check_in_image }}" target="_blank">open image</a></td>
                                    <td><a href="/storage/img/{{ $d->check_out_image }}" target="_blank">open image</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    <div class= "d-flex justify-content-center">
                        {{ $data->links() }}
                    </div>
            </div>
        </div>
    </div>

@endsection

