@extends('layouts.mainadmin')

@section('dashboard')
<div class="container">
    <div class="row">
        <div class="col">
            <a href="/setting/pengaturan-tunjangan">kembali</a>

            @if (session()->has('berhasil'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('berhasil') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif

            @if (session()->has('tidakAdaPenambahanTunjangan'))
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
            {{ session('tidakAdaPenambahanTunjangan') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif

            @if (session()->has('sudahAda'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('sudahAda') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif

            @if (session()->has('berhasilDelete'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('berhasilDelete') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif

            <form action="/setting/pengaturan-tunjangan-karyawan" method="get">
                <div class="col-4 mb-5">
                    <label for="id">Nama</label>
                    <select name="id" id="id" class="form-select mb-3">
                        @foreach ($user as $u)
                        <option value="{{ $u->id }}" {{ request('id') == $u->id ? 'selected' : "" }}>{{ $u->name }}</option>
                        @endforeach
                    </select>
                    <button class="btn btn-primary" type="submit">Search</button>
                </div>
            </form>

            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">Jenis Tunjangan</th>
                    <th scope="col">Nominal</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                    <form action="/pengaturan-tunjangan-karyawan" method="post">
                        <input type="hidden" value="{{ request('id') ? request('id') : auth()->user()->id }}" name="id">
                    @foreach ($tunjangan as $t)
                    <tr>
                        <td>{{ $t->jenis_tunjangan }}</td>
                        <td>{{ $t->nominal }}</td>
                        <td>
                            @csrf
                            <input type="checkbox" name="{{ $t->jenis_tunjangan }}" value="{{ $t->id }}" @foreach ($tunjanganKaryawan as $tk)
                                @if($t->jenis_tunjangan == $tk->jenis_tunjangan)
                                    checked
                                @endif
                            @endforeach>
                        </tr>
                    </td>
                    @endforeach
                        <button type="submit">Submit</button>
                    </form>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
