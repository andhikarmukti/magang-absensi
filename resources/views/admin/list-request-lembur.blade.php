@extends('layouts.mainadmin')

@section('dashboard')
    <div class="container">
        <div class="row">
            <div class="col">
                <h3 class="text-center mb-4 mt-3">List Permintaan Lembur</h3>
                <a href="/list-all-request-lembur" class="btn btn-primary mb-5">List All Request Lembur</a>
                
                @if (session()->has('berhasilAccept'))
                    <div class="alert alert-success alert-dismissible fade show col-6" role="alert">
                    {{ session('berhasilAccept') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                
                @if (session()->has('berhasilDecline'))
                    <div class="alert alert-danger alert-dismissible fade show col-6" role="alert">
                    {{ session('berhasilDecline') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif

                <table class="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col">Nama</th>
                        <th scope="col">Tanggal Lembur</th>
                        <th scope="col">Dari Jam</th>
                        <th scope="col">Hingga Jam</th>
                        <th scope="col">Total Jam Lembur</th>
                        <th scope="col">Nominal Lembur</th>
                        <th scope="col">Keterangan</th>
                        <th scope="col">Approval</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $d)
                        <tr>
                          <th scope="row">{{ $d->name }}</th>
                          <td>{{ $d->tanggal_lembur }}</td>
                          <td>{{ $d->dari_jam_lembur }}</td>
                          <td>{{ $d->hingga_jam_lembur }}</td>
                          <td>{{ $d->total_jam_lembur }} Jam</td>
                          <td>Rp. {{ number_format($d->nominal_lembur,0,',','.') }}</td>
                          <td>{{ $d->keterangan }}</td>
                          <td class="text-muted">{{ $d->approval }}</td>
                          <td>
                              <a href="/accept-request-lembur/{{ $d->id }}" onclick="return confirm('anda yakin?')"><span class="badge bg-success">accept</span></a>
                              <a href="/decline-request-lembur/{{ $d->id }}" onclick="return confirm('anda yakin?')"><span class="badge bg-danger">decline</span></a>
                          </td>
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
@endsection