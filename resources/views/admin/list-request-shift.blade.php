@extends('layouts.mainadmin')

@section('dashboard')
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="text-center">List Request</h1>
                <a class="btn btn-primary mb-4" href="/list-request-shift/all">Lihat Daftar Perubahan Shift</a>

                @if(session()->has('accept'))
                <div class="alert alert-success alert-dismissible fade show mt-4 col-4" role="alert">
                    {{ session('accept') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                  </div>
                @endif

                @if(session()->has('decline'))
                <div class="alert alert-danger alert-dismissible fade show mt-4 col-4" role="alert">
                    {{ session('decline') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                  </div>
                @endif

                <table class="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col">Req. ID</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Entitas</th>
                        <th scope="col">Tanggal</th>
                        <th scope="col">Shift</th>
                        <th scope="col">Jam</th>
                        <th scope="col">Keterangan</th>
                        <th scope="col">Approval</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $d)
                            <tr>
                                <th scope="row">{{ $d->id }}</th>
                                <td>{{ $d->name }}</td>
                                <td>{{ $d->entitas }}</td>
                                <td>{{ $d->tanggal }}</td>
                                <td>{{ $d->shift }}</td>
                                <td>{{ $d->jam }}</td>
                                <td>{{ $d->keterangan }}</td>
                                <td style="color: {{ $d->approval == 'pending' ? 'orange' : "" }}">{{ $d->approval }}</td>
                                <td>
                                    <a onclick="return confirm('Apakah Anda yakin?')" class="badge bg-success" href="/accept-request/{{ $d->id }}"><span></span>Accept</a>
                                    <a onclick="return confirm('Apakah Anda yakin?')" class="badge bg-danger" href="/decline-request/{{ $d->id }}"><span></span>Decline</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
@endsection