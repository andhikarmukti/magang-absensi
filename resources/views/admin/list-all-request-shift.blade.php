@extends('layouts.mainadmin')

@section('dashboard')
<div class="container">
    <div class="row">
        <div class="col">
            <h1 class="text-center mb-4">Seluruh Data Perubahan Shift</h1>
            <a href="/list-request-shift"><span class="badge bg-primary mb-3">Kembali</span></a>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">Req. ID</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Entitas</th>
                    <th scope="col">Tanggal</th>
                    <th scope="col">Shift</th>
                    <th scope="col">Jam</th>
                    <th scope="col">Approval</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($shift as $s)
                        <tr>
                            <th scope="row">{{ $s->id }}</th>
                            <td>{{ $s->name }}</td>
                            <td>{{ $s->entitas }}</td>
                            <td>{{ $s->tanggal }}</td>
                            <td>{{ $s->shift }}</td>
                            <td>{{ $s->jam }}</td>
                            <td style="color: {{ $s->approval == 'decline' ? 'red' : "green" }}">{{ $s->approval }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <div class= "d-flex justify-content-center">
                {{ $shift->links() }}
            </div>
        </div>
    </div>
</div>
@endsection