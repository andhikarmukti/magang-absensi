@extends('layouts.mainadmin')

@section('dashboard')
    <h1>Hitung Gaji</h1>

    @if (session()->has('selesai_hitung'))
    <div class="alert alert-success alert-dismissible fade show mt-4" role="alert">
        {{ session('selesai_hitung') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    <form action="/hitung-gaji" method="post">
        @csrf
        <div class="mb-4">
            <label for="bulan">Bulan </label>
            <select class="form-select col-4 d-inline" id="bulan" name="bulan">
                <option value="01" {{ !$bulan ? 'selected disabled' : 'disabled'}}>--</option>
                <option value="01" {{ $bulan == '01' ? 'selected' : ""}}>Januari</option>
                <option value="02" {{ $bulan == '02' ? 'selected' : ""}}>Februari</option>
                <option value="03" {{ $bulan == '03' ? 'selected' : ""}}>Maret</option>
                <option value="04" {{ $bulan == '04' ? 'selected' : ""}}>April</option>
                <option value="05" {{ $bulan == '05' ? 'selected' : ""}}>Mei</option>
                <option value="06" {{ $bulan == '06' ? 'selected' : ""}}>Juni</option>
                <option value="07" {{ $bulan == '07' ? 'selected' : ""}}>Juli</option>
                <option value="08" {{ $bulan == '08' ? 'selected' : ""}}>Agustus</option>
                <option value="09" {{ $bulan == '09' ? 'selected' : ""}}>September</option>
                <option value="10" {{ $bulan == '10' ? 'selected' : ""}}>Oktober</option>
                <option value="11" {{ $bulan == '11' ? 'selected' : ""}}>November</option>
                <option value="12" {{ $bulan == '12' ? 'selected' : ""}}>Desember</option>
            </select>
        </div>
        <div class="mb-4">
            <label for="tahun">Tahun </label>
            <select class="form-select col-4 d-inline" id="tahun" name="tahun">
                <option value="2021" selected>2021</option>
                <option value="2022">2022</option>
                <option value="2023">2023</option>
                <option value="2024">2024</option>
                <option value="2025">2025</option>
            </select>
        </div>
        <button type="submit" class="btn btn-primary mb-4">Hitung</button>
    </form>
@endsection