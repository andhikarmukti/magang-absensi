@extends('layouts.mainadmin')

@section('dashboard')
<div class="container">
    <div class="row">
        <div class="col">
            <h1 class="text-center">Pengaturan Shift</h1>

            @if(session()->has('berhasilDiubah'))
            <div class="alert alert-warning alert-dismissible fade show col-4" role="alert">
                {{ session('berhasilDiubah') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif

            <form action="/pengaturan-shift" method="get">
                <div class="input-group mb-3 col-4">
                    <input type="text" class="form-control" placeholder="Search.." autofocus name="keyword" value="{{ request('keyword') }}">
                    <button class="btn btn-primary" type="submit" id="button-addon2">Search</button>
                </div>
            </form>

            <table class="table">
                <thead class="">
                  <tr>
                    <th scope="col">Nama</th>
                    <th scope="col">Entitas</th>
                    <th scope="col">Shift</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($data as $d)
                        <tr>
                            <td>{{ $d->name }}</td>
                            <td>{{ $d->entitas }}</td>
                            <td>{{ $d->shift }}</td>
                            <td>
                                <a href="/setting/pengaturan-shift/edit/{{ $d->id }}" class="badge bg-warning text-decoration-none">Edit</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
              </table>
        </div>
    </div>
</div>
@endsection