@extends('layouts.mainadmin')

@section('dashboard')
<div class="container">
    <div class="row">
        <div class="col">

            @if (session()->has('berhasilTambahDataTunjangan'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
            {{ session('berhasilTambahDataTunjangan') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif

            @if (session()->has('delete'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
            {{ session('delete') }}
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endif

            <form method="post" action="/pengaturan-tunjangan">
                @csrf
                <a class="btn btn-warning mb-2" href="/setting/pengaturan-tunjangan-karyawan">Tambah Tunjangan Berdasarkan Karyawan</a>
                <a class="btn btn-info mb-2" href="/setting/pengaturan-tunjangan-berdasarkan-tunjangan">Tambah Tunjangan Berdasarkan Tunjangan</a>
                <hr>
                <div class="mb-3 col-4">
                    <label for="jenis_tunjangan" class="form-label">Jenis Tunjangan</label>
                    <input type="text" class="form-control @error('jenis_tunjangan') is-invalid @enderror" id="jenis_tunjangan" name="jenis_tunjangan" value="{{ old('jenis_tunjangan') }}">
                    @error('jenis_tunjangan')
                    <div id="validationServer03Feedback" class="invalid-feedback">
                          {{ $message }}  
                    </div>
                    @enderror
                </div>
                <div class="mb-3 col-4">
                    <label for="nominal" class="form-label">Nominal Fixed</label>
                    <input type="number" class="form-control" id="nominal" name="nominal">
                </div>
                <div class="mb-3 col-4">
                    <label for="nominal" class="form-label">Nominal Formula (%)</label>
                    <input type="int" class="form-control @error('persentase') is-invalid @enderror" id="persentase" name="persentase" value="{{ old('persentase') }}">
                    @error('persentase')
                    <div id="validationServer03Feedback" class="invalid-feedback">
                          {{ $message }}  
                    </div>
                    @enderror
                    x
                    <select class="form-select @error('pengali_berdasarkan') is-invalid @enderror" aria-label="Default select example" name="pengali_berdasarkan">
                        <option selected disabled>--</option>
                        <option {{ old('pengali_berdasarkan') == 'gaji_pokok' ? 'selected' : "" }} value="gaji_pokok">Gaji Pokok</option>
                        <option {{ old('pengali_berdasarkan') == 'kehadiran' ? 'selected' : "" }} value="kehadiran">Kehadiran</option>\
                      </select>
                      @error('pengali_berdasarkan')
                    <div id="validationServer03Feedback" class="invalid-feedback">
                          {{ $message }}  
                    </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>

            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Jenis Tunjangan</th>
                        <th scope="col">Nominal</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach ($data as $d)
                    <tr>
                        <th scope="row">{{ $d->id }}</th>
                        <td>{{ $d->jenis_tunjangan }}</td>
                        <td>{{ $d->nominal }}</td>
                        <td>
                            <a href="/pengaturan-tunjangan/delete/{{ $d->id }}" class="badge bg-danger border-0" onclick="return confirm('apakah kamu yakin?')">delete</a>
                            <a href="/pengaturan-tunjangan/edit/{{ $d->id }}" class="badge bg-warning">edit</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
