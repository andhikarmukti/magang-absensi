@extends('layouts.mainadmin')

@section('dashboard')
    <div class="container">
        <div class="row">
            <div class="col">
                <h3 class="text-center mb-4 mt-3">List Permintaan Lembur</h3>
                <a href="/list-request-lembur"><span class="mb-4 badge bg-primary">Kembali</span></a>
                <table class="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col">Nama</th>
                        <th scope="col">Tanggal Lembur</th>
                        <th scope="col">Dari Jam</th>
                        <th scope="col">Hingga Jam</th>
                        <th scope="col">Total Jam Lembur</th>
                        <th scope="col">Keterangan</th>
                        <th scope="col">Approval</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $d)
                        <tr>
                          <th scope="row">{{ $d->name }}</th>
                          <td>{{ $d->tanggal_lembur }}</td>
                          <td>{{ $d->dari_jam_lembur }}</td>
                          <td>{{ $d->hingga_jam_lembur }}</td>
                          <td>{{ $d->total_jam_lembur }} Jam</td>
                          <td>{{ $d->keterangan }}</td>
                          <td class="@if($d->approval == 'pending')
                                text-muted
                                @elseif($d->approval == 'accept')
                                text-success
                                @else
                                text-danger
                            @endif">{{ $d->approval }}</td>
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
@endsection