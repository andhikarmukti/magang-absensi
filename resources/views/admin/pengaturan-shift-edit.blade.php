@extends('layouts.mainadmin')

@section('dashboard')
<div class="container">
    <div class="row">
        <div class="col">
            <form action="/pengaturan-shift" method="post">
                @csrf
                @method('PUT')
                <div class="mt-5 col-4">
                    <h4>{{ $data['name'] }}</h4>
                    <input type="hidden" value="{{ $data['id'] }}" name="id">
                    <div class="mb-3">
                        <label for="entitas" class="form-label">Entitas</label>
                        <select class="form-select" name="entitas" id="entitas">
                            @if(!$data['entitas'])
                            <option value="" disabled selected>Silahkan pilih entitas</option>
                            @endif
                            <option value="Artapuri" {{ $data['entitas'] == 'Artapuri' ? 'selected' : ""}}>Artapuri</option>
                            <option value="Bisnison" {{ $data['entitas'] == 'Bisnison' ? 'selected' : ""}}>Bisnison</option>
                            <option value="Marketplace" {{ $data['entitas'] == 'Marketplace' ? 'selected' : ""}}>Marketplace</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="shift" class="form-label">Shift</label>
                        <select class="form-select" name="shift" id="shift">
                            <option value="pagi" {{ $data['shift'] == 'pagi' ? 'selected' : "" }}>Pagi</option>
                            <option value="sore" {{ $data['shift'] == 'sore' ? 'selected' : "" }}>Sore</option>
                        </select>
                    </div>
                    <button class="badge badge-warning border-0" type="submit">Edit</button>
                    <div class="">
                        <a href="/pengaturan-shift" class="badge badge-primary">Kembali</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection