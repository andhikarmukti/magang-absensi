@extends('layouts.mainadmin')

@section('dashboard')
    <div class="container">
        <div class="row">
            <div class="col-6">

              @if (session()->has('berhasil'))
              <div class="alert alert-success alert-dismissible fade show" role="alert">
              {{ session('berhasil') }}
              <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
              @endif

              <a href="/setting/pengaturan-tunjangan">kembali</a>
              <form action="/setting/pengaturan-tunjangan-berdasarkan-tunjangan" method="get">
                <select class="form-select mt-5 mb-3" name="tunjangan_id" id="tunjangan_id">
                        <option selected disabled>Jenis Tunjangan</option>
                        @foreach ($jenis_tunjangan as $jt)
                        <option value="{{ $jt->id }}" {{ request('tunjangan_id') == $jt->id ? 'selected' : ""}}>{{ $jt->jenis_tunjangan }}</option>
                        @endforeach
                      </select>
                      <button type="submit" class="btn btn-primary">Search</button>
                    </form>
              <form action="/pengaturan-tunjangan-karyawan" method="post">
                @csrf
                <div class="btn-group mt-5" aria-label="Basic checkbox toggle button group">
                    <input type="checkbox" class="btn-check" name="set_to_all" id="set_to_all">
                    <label class="btn btn-outline-primary" for="set_to_all">Check All</label>
                  </div>
                <div class="btn-group mt-5" aria-label="Basic checkbox toggle button group">
                    <input type="checkbox" class="btn-check" name="unset_to_all" id="unset_to_all">
                    <label class="btn btn-outline-danger" for="unset_to_all">Uncheck All</label>
                  </div>

                <table class="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($user as $u)
                        <tr>
                          <th scope="row">{{ $u->name }}</th>
                          <td>
                              <input id="checkbox" type="checkbox" name="{{ $u->name }}" value="{{ $u->id }}" @foreach ($tunjangan_karyawan as $tk) @if($tk->user_id == $u->id) checked @endif @endforeach>
                          </td>
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
                  <input type="hidden" value="{{ request('tunjangan_id') }}" name="tunjangan_id">
                  <button class="btn btn-primary" type="submit">Submit</button>
              </form>
            </div>
        </div>
    </div>

<script>
  $('#set_to_all').click(function(){
    if(this.checked) {
      console.log('ok')
      $("input[id=checkbox]").attr('checked', true);
    }
    $('#set_to_all').prop('checked', false);
  })

  $('#unset_to_all').click(function(){
    if(this.checked) {
      console.log('ok')
      $("input[id=checkbox]").attr('checked', false);
    }
    $('#unset_to_all').prop('checked', false);
  })
</script>
@endsection