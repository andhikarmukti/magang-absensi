@extends('layouts.mainadmin')

@section('dashboard')
<div class="container">
    <div class="row">
        <div class="col">
            <h3 class="text-center">List Request Cuti</h3>
            <a class="btn btn-primary mb-4" href="/list-request-cuti/all">Lihat Daftar Request Cuti</a>

            @if (session()->has('acceptRequestCuti'))
                <div class="alert alert-success alert-dismissible fade show col-6" role="alert">
                {{ session('acceptRequestCuti') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif

            @if (session()->has('declineRequestCuti'))
                <div class="alert alert-danger alert-dismissible fade show col-6" role="alert">
                {{ session('declineRequestCuti') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif

            <table class="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">Nama</th>
                    <th scope="col">Dari Tanggal</th>
                    <th scope="col">Hingga Tanggal</th>
                    <th scope="col">Jenis Cuti</th>
                    <th scope="col">Keterangan</th>
                    <th scope="col">Approval</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($data as $d)
                    <tr>
                        <th scope="row">{{ $d->name }}</th>
                        <td>{{ $d->dari_tanggal }}</td>
                        <td>{{ $d->hingga_tanggal }}</td>
                        <td>{{ $d->jenis_cuti }}</td>
                        <td>{{ $d->keterangan }}</td>
                        <td>{{ $d->approval }}</td>
                        <td>
                            <a href="/accept-request-cuti/{{ $d->id }}" class="badge bg-success" onclick="return confirm('anda yakin?')">accept</a>
                            <a href="/decline-request-cuti/{{ $d->id }}" class="badge bg-danger" onclick="return confirm('anda yakin?')">decline</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
        </div>
    </div>
</div>
@endsection
