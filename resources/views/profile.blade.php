<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <title>Profile</title>
    <style>
        img{
            width: 200px;
            border-radius: 20px
        }
    </style>
</head>
<body>
    <a href="/admin"><span class="badge bg-primary">kembali</span></a>
    <br>
    <figure class="figure">
        <img src="/storage/{{ $data[0]['profile_image'] }}" alt="profile_image">
    </figure>
    <h3>Nama : {{ $data[0]['name'] }}</h3>

    @if (session()->has('berhasilUbahGambar'))
        <div class="alert alert-primary alert-dismissible fade show col-6" role="alert">
        {{ session('berhasilUbahGambar') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    @if (session()->has('gambarKosong'))
        <div class="alert alert-warning alert-dismissible fade show col-6" role="alert">
        {{ session('gambarKosong') }}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif

    <form action="/profile" method="post" enctype="multipart/form-data">
        @csrf
        <input type="hidden" value="{{ $data[0]['profile_image'] }}" name="profile_image_lama">
        <input class="@error('profile_image') is-invalid @enderror" id="profile_image" type="file" name="profile_image" onchange="imagePreview()">
        <button type="submit" class="btn btn-primary">Submit</button>
        <img src="" class="profile_image_preview">
    </form>

    <script>
        function imagePreview(){
            const image = document.querySelector('#profile_image')
            const imgPreview = document.querySelector('.profile_image_preview')

            imgPreview.style.display = 'block';

            const oFReader = new FileReader();
            oFReader.readAsDataURL(image.files[0]);

            oFReader.onload = function(oFREvent){
                imgPreview.src = oFREvent.target.result;
            }
        }
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>
    