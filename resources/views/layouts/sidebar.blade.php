<input type="checkbox" id="nav-toggle">
    <div class="sidebar">
        <div class="sidebar-logo">
            <h2>  <img src="img/artapuri.png" alt=""><span>Attendance</span></h2>
        </div>
        <!--Ini Bagian Menu nih-->
        <div class="sidebar-menu">
            <ul>
                <li>
                    <a href="/dashboard" class="{{ Request::is('dashboard*') ? 'active' : '' }} text-decoration-none"><span class="las la-home"></span>
                    <span>Home</span></a>
                </li>
                <li>
                    <a href="/attendance-list" class="{{ Request::is('attendance-list*') ? 'active' : '' }} text-decoration-none"><span class="las la-clipboard-list"></span>
                    <span>Attendance List</span></a>
                </li>
                <li>
                    <a href="history" class="{{ Request::is('history*') ? 'active' : '' }} text-decoration-none"><span class="las la-history"></span>
                    <span>History</span></a>
                </li>
                <li>
                    <a href="/list-request-shift" class="{{ Request::is('list-request-shift*') ? 'active' : '' }} text-decoration-none"><span class="las la-calendar-plus"></span>
                    <span>Shift Schedule</span></a>
                </li>
                <li>
                    <a href="/holiday" class="{{ Request::is('holiday*') ? 'active' : '' }} text-decoration-none"><span class="las la-calendar"></span>
                    <span>Holiday List</span></a>
                </li>
                <li>
                    <a href="#"><span class="las la-user-clock"></span>
                    <span>Request Shift</span></a>
                </li>
                <li>
                    <a href="/request-cuti" class="{{ Request::is('request-cuti*') ? 'active' : '' }} text-decoration-none"><span class="las la-user-lock"></span>
                    <span>Request Leave</span></a>
                </li>
                <li>
                    <a href="/list-request-lembur"><span class="las la-stopwatch"></span>
                    <span>Overtime List Request</span></a>
                </li>
                <li>
                    <a href="/request-koreksi-kehadiran"><span class="las la-stopwatch"></span>
                    <span>Attendance Correction List Request</span></a>
                </li>
                <li>
                    <a href="/departemen" class="{{ Request::is('departemen*') ? 'active' : '' }}"><span class="las la-tools"></span>
                    <span>Departemen</span></a>
                </li>
                <li>
                    <a href="/setting" class="{{ Request::is('setting*') ? 'active' : '' }}"><span class="las la-tools"></span>
                    <span>Setting</span></a>
                </li>
            </ul>
        </div>
    </div>

