@extends('layouts.mainadmin')

@section('dashboard')
<div class="container">
    <div class="row">
        <div class="col">
            @if(auth()->user()->role == 'admin')
            <h3 class="text-center mt-4">History Absen {{ $company->nama_company }}</h3>
            @else
            <h3 class="text-center mt-4">History Absen {{ auth()->user()->name }}</h3>
            @endif
            <p>Sort By :</p>
            <form action="/attendance-list" method="get">
                @if(auth()->user()->role == 'admin')
                <div class="mb-4">
                    <label for="bulan">Nama</label>
                    <select class="form-select col-4 d-inline" id="id" name="id">
                        @foreach ($user as $u)
                        <option value="{{ $u->id }}" {{ $u->id == auth()->user()->id ? 'selected' : ""}}> {{ $u->name }} </option>
                        @endforeach
                    </select>
                </div>
                @endif
                <div class="mb-4">
                    <label for="bulan">Bulan </label>
                    <select class="form-select col-4 d-inline" id="bulan" name="bulan">
                        <option value="01" {{ !$bulan ? 'selected disabled' : 'disabled' }}>--</option>
                        <option value="01" {{ $bulan=='01' ? 'selected' : "" }}>Januari</option>
                        <option value="02" {{ $bulan=='02' ? 'selected' : "" }}>Februari</option>
                        <option value="03" {{ $bulan=='03' ? 'selected' : "" }}>Maret</option>
                        <option value="04" {{ $bulan=='04' ? 'selected' : "" }}>April</option>
                        <option value="05" {{ $bulan=='05' ? 'selected' : "" }}>Mei</option>
                        <option value="06" {{ $bulan=='06' ? 'selected' : "" }}>Juni</option>
                        <option value="07" {{ $bulan=='07' ? 'selected' : "" }}>Juli</option>
                        <option value="08" {{ $bulan=='08' ? 'selected' : "" }}>Agustus</option>
                        <option value="09" {{ $bulan=='09' ? 'selected' : "" }}>September</option>
                        <option value="10" {{ $bulan=='10' ? 'selected' : "" }}>Oktober</option>
                        <option value="11" {{ $bulan=='11' ? 'selected' : "" }}>November</option>
                        <option value="12" {{ $bulan=='12' ? 'selected' : "" }}>Desember</option>
                    </select>
                </div>
                <div class="mb-4">
                    <label for="tahun">Tahun </label>
                    <select class="form-select col-4 d-inline" id="tahun" name="tahun">
                        <option value="2021" selected>2021</option>
                        <option value="2022">2022</option>
                        <option value="2023">2023</option>
                        <option value="2024">2024</option>
                        <option value="2025">2025</option>
                    </select>
                </div>
                <button class="btn btn-primary mb-4">Search</button>
            </form>

            {{-- history slip gaji --}}
            <div class="card w-50" style="width: 18rem;">
                <div class="card-header">
                    <h4>Total Gaji : Rp {{ number_format($slip_gaji->pluck('nominal')->sum(),0,',','.') }}</h4>
                </div>
                <ul class="list-group list-group-flush">
                    @foreach ($slip_gaji as $sp)
                    <li class="list-group-item">{{ $sp->jenis_pendapatan }} : <h5>Rp {{
                            number_format($sp->nominal,0,',','.') }}</h5>
                    </li>
                    @endforeach
                </ul>
            </div>

            <a class="btn btn-success mt-5" href="{{ route('absen-export') }}">Export data</a>
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th scope="col">Nama</th>
                        <th scope="col">Shift</th>
                        <th scope="col">Check In</th>
                        @if($perubahan_check_in > 0)
                        <th scope="col">Perubahan Check In</th>
                        @endif
                        <th scope="col">Check Out</th>
                        <th scope="col">Total Jam Kerja</th>
                        <th scope="col">keterangan</th>
                        <th scope="col">Check In Image</th>
                        <th scope="col">Check Out Image</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @if($data)
                    @foreach ($data as $d)
                    <tr>
                        <td>{{ $d->name }}</td>
                        <td>{{ $d->shift }}</td>
                        <td>{{ $d->check_in }}</td>
                        @if($perubahan_check_in > 0)
                        <td>{{ $d->perubahan_check_in }}</td>
                        @endif
                        <td>{{ $d->check_out }}</td>
                        <td>{{ $d->selisih_waktu }}</td>
                        <th scope="row" style="{{ $d->keterangan == 'telat' ? 'color: red' : 'color: green' }}">{{
                            $d->keterangan }}</th>
                        <td><a href="/storage/img/{{ $d->check_in_image }}" target="_blank">open image</a></td>
                        <td><a href="/storage/img/{{ $d->check_out_image }}" target="_blank">open image</a></td>
                        <td>
                            <a href="/pengajuan-perubahan-absen/{{ $d->id }}"
                                class="badge bg-warning text-decoration-none">Pengajuan Perubahan</a>
                        </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
            <hr>
            <h3>List Lembur</h3>
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th scope="col">Nama</th>
                        <th scope="col">Tanggal Lembur</th>
                        <th scope="col">Dari Jam</th>
                        <th scope="col">Hingga Jam</th>
                        <th scope="col">Total Jam Lembur</th>
                        <th scope="col">Nominal Lembur</th>
                        <th scope="col">Approval</th>
                    </tr>
                </thead>
                <tbody>
                    @if($lembur)
                    @foreach ($lembur as $l)
                    <tr>
                        <td>{{ $l->name }}</td>
                        <td>{{ $l->tanggal_lembur }}</td>
                        <td>{{ $l->dari_jam_lembur }}</td>
                        <td>{{ $l->hingga_jam_lembur }}</td>
                        <td>{{ $l->total_jam_lembur }} Jam</td>
                        <td>Rp {{ number_format($l->nominal_lembur,0,',','.') }}</td>
                        <td>{{ $l->approval }}</td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection