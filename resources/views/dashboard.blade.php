@extends('layouts.mainadmin')

@section('dashboard')
    <div class="cards">
        <div class="card-single">
            <div>
                <h1>{{ $totalKaryawan }}</h1>
                <span>Total Karyawan</span>
            </div>
            <div>
                <span class="las la-users"></span>
            </div>
        </div>
        
        <div class="card-single">
            <div>
                <h1>250 Jam</h1>
                <span>Total Jam Kerja</span>
            </div>
            <div>
                <span class="las la-clock"></span>
            </div>
        </div>

        <div class="card-single">
            <div>
                <h1>25 Jam</h1>
                <span>Total Telat</span>
            </div>
            <div>
                <span class="las la-clock"></span>
            </div>
        </div>
        
        <div class="card-single">
            <div>
                <h1>12  </h1>
                <span>Total Cuti</span>
            </div>
            <div>
                <span class="lab la-wpforms"></span>
            </div>
        </div>
    </div>
    
        <div class="employes">
            
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-success" href="{{ route('user-export') }}">Export data</a>
                    <h3>Daftar Karyawan</h3>
                    
                    <button>Lihat Semua<span class="las la-arrow-right">
                    </span></button>
                </div>

                <div class="card-body">

                    @foreach ($karyawan as $k)
                    <div class="employe">
                        <div class="info">
                            <img src="img/avatars/1.png" width="40px" height="40px" alt="">
                            <div>
                                <h4>{{ $k->name }}</h4>
                                <small>{{ $k->entitas }}</small>
                                <small> | {{ $k->jabatan }}</small>
                            </div>
                        </div>
                        <div class="contact">
                            <span class="las la-user-circle"></span>
                            <span class="lab la-whatsapp"></span>
                            <span class="las la-phone"></span>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>

    </div>
@endsection

