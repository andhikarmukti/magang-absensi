@extends('layouts.mainadmin')

@section('dashboard')
    <div class="container">
        <div class="row">
            <div class="col">

                @if (session()->has('berhasilAcceptPerubahan'))
                    <div class="alert alert-success alert-dismissible fade show col-6" role="alert">
                    {{ session('berhasilAcceptPerubahan') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                
                @if (session()->has('declinePerubahan'))
                    <div class="alert alert-danger alert-dismissible fade show col-6" role="alert">
                    {{ session('declinePerubahan') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif

                <table class="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col">Nama</th>
                        <th scope="col">Awal Check In</th>
                        <th scope="col">Perubahan Check In</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($data as $d)
                        <tr>
                          <th scope="row">{{ $d->name }}</th>
                          <td>{{ $d->check_in }}</td>
                          <td>{{ $d->perubahan_check_in }}</td>
                          <td>
                              <a href="/accept-perubahan/{{ $d->id }}" class="badge bg-success" onclick="return confirm('Anda yakin?')">Accept</a>
                              <a href="/decline-perubahan/{{ $d->id }}" class="badge bg-danger" onclick="return confirm('Anda yakin?')">Decline</a>
                          </td>
                        </tr>
                        @endforeach
                    </tbody>
                  </table>
            </div>
        </div>
    </div>
@endsection