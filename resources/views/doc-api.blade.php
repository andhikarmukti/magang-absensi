@extends('layouts.main')

@section('container')
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card border-primary mb-3 mt-4" style="max-width: 24rem;">
                <div class="card-header">Read All Absen</div>
                <div class="card-body text-primary">
                    <h6 class="card-title">url : https://magang.artapuri.com/api/absenall</h6>
                    <h6 class="card-title">method : GET</h6>
                    <h6 class="card-title">user : admin</h6>
                    <h6 class="card-title">key : AbsensiMagang</h6>
                </div>
              </div>
              <div class="card border-secondary mb-3" style="max-width: 24rem;">
                <div class="card-header">Read Absen by ID</div>
                <div class="card-body text-secondary">
                    <h6 class="card-title">url : https://magang.artapuri.com/api/absen</h6>
                    <h6 class="card-title">method : GET</h6>
                    <h6 class="card-title">user : admin</h6>
                    <h6 class="card-title">key : AbsensiMagang</h6>
                    <h6 class="card-title">id : Your ID</h6>
                </div>
              </div>
              <div class="card border-success mb-3" style="max-width: 24rem;">
                <div class="card-header">Create Data Absen</div>
                <div class="card-body text-success">
                    <h6 class="card-title">url : https://magang.artapuri.com/api/absen</h6>
                    <h6 class="card-title">method : POST</h6>
                    <h6 class="card-title">user : admin</h6>
                    <h6 class="card-title">key : AbsensiMagang</h6>
                    <h6 class="card-title">name : Your Name</h6>
                    <h6 class="card-title">image : Your Base64 Image</h6>
                    <h6 class="card-title">shift : Your Base64 Image</h6>
                    <h6 class="card-title">id : Your ID</h6>
                    <h6 class="card-title">perubahan_jam : Your perubahan_jam</h6>
                </div>
              </div>
        </div>
    </div>
</div>
@endsection