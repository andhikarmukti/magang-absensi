@extends('layouts.mainadmin')

@section('dashboard')
<div class="container">
    <div class="row">
        <div class="col">
            <h1 class="mb-4 text-center">pengajuan cuti</h1>
            <h5>Jatah cuti : {{ $jatah_cuti }} hari</h5>

            @if (session()->has('berhasilSubmitCuti'))
                <div class="alert alert-success alert-dismissible fade show col-6" role="alert">
                {{ session('berhasilSubmitCuti') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif

            @if (session()->has('tidakBisaCuti'))
                <div class="alert alert-danger alert-dismissible fade show col-7" role="alert">
                {{ session('tidakBisaCuti') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            @endif

            <form action="request-cuti" method="post" class="col-5">
                @csrf
                <div class="mb-3">
                  <input type="hidden" class="form-control" id="user_id" name="user_id" value="{{ auth()->user()->id }}">
                  <input type="hidden" class="form-control" id="name" name="name" value="{{ auth()->user()->name }}">
                </div>
                <div class="mb-3">
                  <label for="dari_tanggal" class="form-label">Dari Tanggal</label>
                  <input type="date" class="form-control @error('dari_tanggal') is-invalid @enderror" id="dari_tanggal" name="dari_tanggal">
                  <div id="validationServer03Feedback" class="invalid-feedback">
                    @error('dari_tanggal')
                    {{ $message }}
                    @enderror
                  </div>
                </div>
                <div class="mb-3">
                  <label for="hingga_tanggal" class="form-label">Hingga Tanggal</label>
                  <input type="date" class="form-control @error('hingga_tanggal') is-invalid @enderror" id="hingga_tanggal" name="hingga_tanggal">
                  <div id="validationServer03Feedback" class="invalid-feedback">
                    @error('hingga_tanggal')
                    {{ $message }}
                    @enderror
                  </div>
                </div>
                <div class="mb-3">
                  <label for="jenis_cuti" class="form-label">Jenis Cuti</label>
                  <select name="jenis_cuti" id="jenis_cuti" class="form-select @error('jenis_cuti') is-invalid @enderror">
                    <option selected disabled>--</option>
                    <option value="Cuti Karyawan">Cuti Karyawan</option>
                    <option value="Cuti Menikah">Cuti Menikah</option>
                    <option value="Cuti Hamil">Cuti Hamil</option>
                  </select>
                  <div id="validationServer03Feedback" class="invalid-feedback">
                    @error('jenis_cuti')
                    {{ $message }}
                    @enderror
                  </div>
                </div>
                <div class="mb-3">
                    <label for="keterangan" class="form-label">Keterangan</label>
                    <input type="text" class="form-control @error('keterangan') is-invalid @enderror" id="keterangan" name="keterangan">
                    <div id="validationServer03Feedback" class="invalid-feedback">
                      @error('keterangan')
                    {{ $message }}
                  </div>
                    @enderror
                  </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
        </div>
    </div>
</div>
@endsection
