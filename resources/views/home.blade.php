@extends('layouts.main')

@section('container')
<div class="container">
    <div class="row mt-3">
        <div class="col-lg">
            
            @if (session()->has('berhasilCheckIn'))
            <div class="alert alert-success alert-dismissible fade show mt-4" role="alert">
                {{ session('berhasilCheckIn') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
            @endif
            
            @if (session()->has('berhasilCheckOut'))
            <div class="alert alert-warning alert-dismissible fade show mt-4" role="alert">
                {{ session('berhasilCheckOut') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
            @endif

            @if (session()->has('wajibFoto'))
            <div class="alert alert-danger alert-dismissible fade show mt-4" role="alert">
                {{ session('wajibFoto') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
            @endif

            @if (session()->has('jarakTerlaluJauh'))
            <div class="alert alert-danger alert-dismissible fade show mt-4" role="alert">
                {{ session('jarakTerlaluJauh') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
            @endif

            <h1 class="h2">Hallo, {{ auth()->user()->name }}!</h1>
            <a class="btn btn-outline-primary mb-3" href="/request-shift">Request Shift</a>
            <a class="btn btn-outline-primary mb-3" href="/profile/{{ auth()->user()->id }}">Profile</a>
            <p>Shift : {{ $userShift }}</p>
            <p>Jam kerja : {{ $jamShift }}</p>
            <form action="/absen" method="post">
                @csrf
                <div id="my_camera" class="d-flex justify-content-center"></div>
                <br/>
                <input type="hidden" name="image" class="image-tag"> {{-- base64 --}}
                <input type="button" value="Take Photo" class="btn btn-warning" name="name" onClick="take_snapshot()">
                <button type="submit" class="btn btn-primary" @if($cek_checkout2 == 1) onclick="return confirm('apakah anda yakin akan melakukan checkout kembali?')" @endif>Submit</button>
                <div class="col-md-12 text-center">
                preview
                <div class="col-lg" id="results"></div>
            </form>
        </div>
    </div>
</div>


<script language="JavaScript">
    Webcam.set({
        width: 800,
        height: 600,
        image_format: 'jpeg',
        jpeg_quality: 90
    });

    Webcam.attach( '#my_camera' );

    function take_snapshot() {
        Webcam.snap( function(data_uri) {
            $(".image-tag").val(data_uri);
            document.getElementById('results').innerHTML = '<img src="'+data_uri+'"/>';
        } );
    }
  </script>
@endsection