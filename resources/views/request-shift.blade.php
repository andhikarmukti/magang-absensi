<!DOCTYPE html>
<html>
<head>
    <title>Request Shift</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row mt-5">
            <div class="col">
                <h4 class="mb-4">{{ auth()->user()->name }}</h4>

                @if(session()->has('berhasilTambahDataShift'))
                <div class="alert alert-success alert-dismissible fade show mt-4 col-4" role="alert">
                    {{ session('berhasilTambahDataShift') }}
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                  </div>
                @endif

                <div class="card p-3 mb-2 col-lg-7">
                    <form action="/request-shift" method="post">
                        @csrf
                        <div class="mb-3 row">
                            <label for="tanggal" class="col-sm-2 col-form-label">Tanggal :</label>
                            <div class="col-sm-5">
                            <input type="date" class="form-control" id="tanggal" name="tanggal">
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="shift" class="col-sm-2 col-form-label">Shift :</label>
                            <div class="col-sm-5">
                            <select class="form-select" name="shift" id="shift">
                                <option value="pagi" {{ $user['shift'] == 'pagi' ? 'selected' : "" }}>Pagi</option>
                                <option value="sore" {{ $user['shift'] == 'sore' ? 'selected' : "" }}>Sore</option>
                            </select>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="jam" class="col-sm-2 col-form-label">Jam :</label>
                            <div class="col-sm-5">
                                <input type="time" class="form-control" id="jam" name="jam">
                                <div id="timeHelp" class="form-text text-warning">Kosongkan jika mengikuti jam normal shift</div>
                            </div>
                        </div>
                        <div class="mb-3 row">
                            <label for="keterangan" class="col-sm-2 col-form-label">Keterangan :</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" id="keterangan" name="keterangan">
                            </div>
                        </div>
                        <button class="btn btn-primary mb-5" type="submit">Request</button>
                    </form>
                </div>
                <a href="/"><span class="badge bg-primary">Kembali</span></a>
                
                <table class="table table-hover">
                    <thead>
                      <tr>
                        <th scope="col">Req. ID</th>
                        <th scope="col">Nama</th>
                        <th scope="col">Entitas</th>
                        <th scope="col">Tanggal</th>
                        <th scope="col">Shift</th>
                        <th scope="col">Approval</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($shift as $s)
                            <tr>
                                <th scope="row">{{ $s->id }}</th>
                                <td>{{ $s->name }}</td>
                                <td>{{ $s->entitas }}</td>
                                <td>{{ $s->tanggal }}</td>
                                <td>{{ $s->shift }}</td>
                                <td style="color: {{ $s->approval == 'pending' ? 'orange' : "" }}">{{ $s->approval }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                  </table>

            </div>
        </div>
    </div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</body>
</html>