@extends('layouts.mainadmin')

@section('dashboard')
<div class="container">
    <div class="row">

        
        <div class="col-4 mt-5">
            <a class="btn btn-primary mb-5" href="/departemen/tambah-ke-karyawan">Tambahkan ke Karyawan</a>

            @if(session()->has('store'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('store') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
            @endif

            <form method="POST" action="/departemen">
                @csrf
                <div class="mb-3">
                  <label for="nama_departemen" class="form-label">Nama Departemen</label>
                  <input type="text" class="form-control" id="nama_departemen" name="nama_departemen">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
        </div>
    </div>
</div>

@endsection