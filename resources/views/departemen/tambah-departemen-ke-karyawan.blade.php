@extends('layouts.mainadmin')

@section('dashboard')
<div class="container">
    <div class="row">

        
        <div class="col-4 mt-5">
            <a class="btn btn-primary mb-5" href="/departemen">kembali</a>

            @if(session()->has('update'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('update') }}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
              </div>
            @endif

            <form method="POST" action="/departemen/tambah-ke-karyawan/store">
                @csrf
                <div class="mb-3">
                    <select class="form-select" aria-label="Default select example" name="id" id="id">
                        <option selected disabled>Nama Karyawan</option>
                        @foreach ($user as $u)
                        <option value="{{ $u->id }}">{{ $u->name }}</option>
                        @endforeach
                      </select>
                </div>
                <div class="mb-3">
                    <select class="form-select" aria-label="Default select example" name="departemen_id" id="departemen_id">
                        <option selected disabled>Nama Departemen</option>
                        @foreach ($departemen as $d)
                        <option value="{{ $d->id }}">{{ $d->nama_departemen }}</option>
                        @endforeach
                      </select>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
        </div>
    </div>
</div>
@endsection