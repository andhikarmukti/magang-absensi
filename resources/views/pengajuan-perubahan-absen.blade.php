@extends('layouts.mainadmin')

@section('dashboard')
<div class="container">
    <div class="row">
        <div class="col">
            <table class="table table-hover">
                <thead>
                    <a href="/attendance-list" class="badge bg-primary mb-3">kembali</a>
                    <br>

                    @if (session()->has('berhasilUpdateDataAbsen'))
                        <div class="alert alert-success alert-dismissible fade show col-6" role="alert">
                        {{ session('berhasilUpdateDataAbsen') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif

                    <form action="/pengajuan-perubahan-absen" method="post">
                      @csrf
                      <div class="col-4">
                      <label for="date">Tanggal</label>
                      <input type="text" readonly value="{{ $tanggal }}" class="form-control mb-3" name="date" id="date">
                      <input type="hidden" value="{{ $data[0]['id'] }}" name="id">
                    </div>
                    <div class="col-4">
                      <label for="time" class="form-label">Check In</label>
                      <input class="form-control col-4" type="time" name="time" id="time">
                    </div>
                    <input class="form-control col-4" type="hidden" value="koreksi kehadiran pending" name="keterangan">
                    <button class="btn btn-primary mt-3" type="submit">Submit</button>
                  </form>
                </tbody>
              </table>
        </div>
    </div>
</div>
@endsection