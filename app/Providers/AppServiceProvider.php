<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Paginator::useBootstrap();
        if(auth()->user()){
            view()->composer('*', function ($view){
                $company = User::where('id', auth()->user()->id)->first()->company;
                $user = User::where('company_id', $company->id)->get();;
                $view->with([
                    'company', $company,
                    'user' => $user
                ]);
            });
        }
    }
}
