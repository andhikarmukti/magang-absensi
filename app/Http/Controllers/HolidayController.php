<?php

namespace App\Http\Controllers;

use App\Models\Holiday;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class HolidayController extends Controller
{
    public function holiday()
    {
        $curl = curl_init();
        $url = "https://api-harilibur.vercel.app/api";

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($curl);
        $result = json_decode($result);
        curl_close($curl);

        for($i = 0; $i < count($result); $i++){
            Holiday::create([
                'holiday_date' => $result[$i]->holiday_date,
                'holiday_name' => $result[$i]->holiday_name,
                'is_national_holiday' => $result[$i]->is_national_holiday,
            ]);
        }

        return 'ok sip';

    }

    public function update(Request $request)
    {
        Holiday::where('id', $request->id)->update([
            'holiday_date' => $request->tanggal
        ]);

        return back()->with('berhasilUpdate', 'Berhasil merubah tanggalan hari libur nasional');
    }
}
