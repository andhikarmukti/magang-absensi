<?php

namespace App\Http\Controllers;

use App\Models\Absen;
use Illuminate\Http\Request;
use App\Exports\AbsensExport;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class AbsenController extends Controller
{
    public function absen(Request $request)
    {
        //pengecekan lokasi
        // $latitude1 = "-6.026194";
        // $longitude1 =  "106.046944";
        // $latitude2 = "-5.8887767";
        // $longitude2 = "106.0354";
        // $earth_radius = 6371; // earth radius in km

        // $dLat = deg2rad($latitude2 - $latitude1);
        // $dLon = deg2rad($longitude2 - $longitude1);

        // $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);
        // $c = 2 * asin(sqrt($a));
        // $distance = $earth_radius * $c;
        // $distance = $distance * 1000; // satuan meter
        // dd($distance);
        // if($distance > 100){
        //     return back()->with('jarakTerlaluJauh', 'jarak terlalu jauh');
        // }

        // dd([
        //     $distance,
        //     $a,
        //     $c,
        //     $dLat,
        //     $dLon
        // ]);
        
        if($request->image == null){
            return back()->with('wajibFoto', 'Absen disini wajib melakukan foto selfie');
        }

        //cek sudah absen atau belum?
        $today = Carbon::now()->format('Y-m-d');
        $cekAbsenToday = DB::table('absens')->where('user_id', auth()->user()->id)
                                            ->whereDate('created_at', $today)
                                            ->count();

        //cek apakah sudah upload foto checkin
        $cekFotoCheckin = DB::table('absens')->where('user_id', auth()->user()->id)
                                            ->whereDate('created_at', $today)
                                            ->pluck('check_in_image');
                                            // dd($cekFotoCheckin);

        // upload data base64                                    
        $img = $request->image;
        $folderPath = "storage/img/"; 
        $image_parts = explode(";base64,", $img);
        $image_type_aux = explode("image/", $image_parts[0]);
        $image_type = $image_type_aux[1]; 
        $image_base64 = base64_decode($image_parts[1]);
        $fileName = uniqid() . '.png';
        $file = $folderPath . $fileName;
        file_put_contents($file, $image_base64);

        //menghitung selisih waktu
        if(DB::table('absens')->where('user_id', auth()->user()->id)
        ->whereDate('created_at', $today)
        ->pluck('check_in')->count() > 0){
            DB::table('absens')->where('user_id', auth()->user()->id)
            ->whereDate('created_at', $today)
            ->update([
                'check_out' => Carbon::now()
            ]);
            $checkin = DB::table('absens')->where('user_id', auth()->user()->id)
                                            ->whereDate('created_at', $today)
                                            ->pluck('check_in');
            $checkout = DB::table('absens')->where('user_id', auth()->user()->id)
                                            ->whereDate('created_at', $today)
                                            ->pluck('check_out');
                                            $selisihJamKerja = strtotime($checkout[0]) - strtotime($checkin[0]);
                                            $totalJamKerja = gmdate("H:i:s", $selisihJamKerja);
                                        }
        //pengecekan sudah ada submit di hari ini atau belum
        if($cekAbsenToday == 0){
            Absen::create([
                'name' => auth()->user()->name,
                'check_in' => Carbon::now(),
                'jam_check_in' => date_format(date_create(Carbon::now()), "H:i:s"),
                'user_id' => auth()->user()->id,
                'check_in_image' => $fileName
            ]);
        }else{
        //menghapus foto checkout yang lama sebelum mengganti dengan yang baru
            $fotoCheckOut = DB::table('absens')->where('user_id', auth()->user()->id)
            ->whereDate('created_at', $today)
            ->pluck('check_out_image');
            
            if($fotoCheckOut[0] != 'default.jpg'){
                Storage::delete('img/' . $fotoCheckOut[0]);
            };
            
        //jika sudah ada submit, maka submit berikutnya akan jadi checkout
            DB::table('absens')->where('user_id', auth()->user()->id)
            ->whereDate('created_at', $today)
            ->update([
                'check_out' => Carbon::now(),
                'jam_check_out' => date_format(date_create(Carbon::now()), "H:i:s"),
                'check_out_image' => $fileName,
                'selisih_waktu' => $totalJamKerja
            ]);
            return back()->with('berhasilCheckOut', 'Berhasil melakukan CHECK OUT');
        }

        //update keterangan telat atau good berdasarkan data checkin yang sudah masuk ke db
        //cek apakah ada perubahan shift atau tidak
        if(auth()->user()->perubahan_shift == null){
            $shift = auth()->user()->shift;
        }else{
            $shift = auth()->user()->perubahan_shift;
        }

        //cek apakah ada perubahan jam kerja
        if(auth()->user()->perubahan_jam == null){
            $jamMasukShiftPagi = "08:00:00";
            $jamMasukShiftSiang = "14:00:00";
            $jamMasukShiftSore = "16:00:00";
        }else{
            $jamKerja = auth()->user()->perubahan_jam;
            $jamMasukShiftPagi = $jamKerja;
            $jamMasukShiftSiang = $jamKerja;
            $jamMasukShiftSore = $jamKerja;
        }
            
            $data = DB::table('absens')->where('user_id', auth()->user()->id)
            ->whereDate('created_at', $today)
            ->get()->all();

            if($shift == 'pagi'){
                if(date("H:i:s", strtotime($data[0]->check_in)) > $jamMasukShiftPagi){
                DB::table('absens')->where('user_id', auth()->user()->id)
                ->whereDate('created_at', $today)
                ->update([
                        'keterangan' => 'telat',
                        'shift' => $shift
                    ]);
                }else{
                DB::table('absens')->where('user_id', auth()->user()->id)
                ->whereDate('created_at', $today)
                ->update([
                        'keterangan' => 'good',
                        'shift' => $shift
                    ]);
                }
            }elseif($shift == 'siang'){
                if(date("H:i:s", strtotime($data[0]->check_in)) > $jamMasukShiftSiang){
                DB::table('absens')->where('user_id', auth()->user()->id)
                ->whereDate('created_at', $today)
                ->update([
                        'keterangan' => 'telat',
                        'shift' => $shift
                    ]);
                }else{
                DB::table('absens')->where('user_id', auth()->user()->id)
                ->whereDate('created_at', $today)
                ->update([
                        'keterangan' => 'good',
                        'shift' => $shift
                    ]);
                }
            }elseif($shift == 'sore'){
                if(date("H:i:s", strtotime($data[0]->check_in)) > $jamMasukShiftSore){
                DB::table('absens')->where('user_id', auth()->user()->id)
                ->whereDate('created_at', $today)
                ->update([
                        'keterangan' => 'telat',
                        'shift' => $shift
                    ]);
                }else{
                DB::table('absens')->where('user_id', auth()->user()->id)
                ->whereDate('created_at', $today)
                ->update([
                        'keterangan' => 'good',
                        'shift' => $shift
                    ]);
                }
            }

        return back()->with('berhasilCheckIn', 'Berhasil melakukan CHECK IN');
    }

    //hanya untuk testing memasukkan data ke databse
    public function isiKeterangan()
    {
        $shift = auth()->user()->shift;
        $totalData = Absen::all()->max('id');
        $jamMasukShiftPagi = "08:00:00";
        $jamMasukShiftSore = "16:00:00";
        // dd($totalData);

        for($i = 1; $i <= $totalData; $i++){
            $data = Absen::where('id', $i)->get()->all();
            if($data !== []){
                if($shift == 'pagi'){
                    if(date("H:i:s", strtotime($data[0]->check_in)) > $jamMasukShiftPagi){
                        Absen::find($i)->update([
                            'keterangan' => 'telat'
                        ]);
                    }else{
                        Absen::find($i)->update([
                            'keterangan' => 'good'
                        ]);
                    }
                }else{
                    if(date("H:i:s", strtotime($data[0]->check_in)) > $jamMasukShiftSore){
                        Absen::find($i)->update([
                            'keterangan' => 'telat'
                        ]);
                    }else{
                        Absen::find($i)->update([
                            'keterangan' => 'good'
                        ]);
                    }
                }
            }
        }
    }

    
    public function editPengajuanPerubahanAbsen(Request $request)
    {
        $check_in = $request->date . ' ' . $request->time;
        Absen::where('id' ,$request->id)->update([
            'keterangan' => $request->keterangan,
            'perubahan_check_in' => $check_in
        ]);

        return back()->with('berhasilUpdateDataAbsen', 'Data berhasil disubmit, silahkan tunggu konfirmasi');
    }

    public function acceptPerubahan($id)
    {
        $data = Absen::where('id', $id)->get()->toArray();
        // dd($data[0]['check_in']);

        Absen::find($id)->update([
            'check_in' => $data[0]['perubahan_check_in']
        ]);
        
        $jamMasukShiftPagi = "08:00:00";
        $jamMasukShiftSiang = "14:00:00";
        $jamMasukShiftSore = "16:00:00";

        // dd(date("H:i:s", strtotime($data[0]['check_in'])) > $jamMasukShiftSore);
        
        if($data[0]['shift'] == 'pagi'){
            // dd($data[0]['check_in']);
            if(date("H:i:s", strtotime($data[0]['perubahan_check_in'])) > $jamMasukShiftPagi){
                Absen::find($id)->update([
                    'keterangan' => 'telat',
                    'perubahan_check_in' => null
                ]);
            }else{
                Absen::find($id)->update([
                    'keterangan' => 'good',
                    'perubahan_check_in' => null
                ]);
            }
        }elseif($data[0]['shift'] == 'sore'){
            if(date("H:i:s", strtotime($data[0]['perubahan_check_in'])) > $jamMasukShiftSore){
                Absen::find($id)->update([
                    'keterangan' => 'telat',
                    'perubahan_check_in' => null
                ]);
            }else{
                Absen::find($id)->update([
                    'keterangan' => 'good',
                    'perubahan_check_in' => null
                ]);
            }
        }elseif($data[0]['shift'] == 'siang'){
            if(date("H:i:s", strtotime($data[0]['perubahan_check_in'])) > $jamMasukShiftSiang){
                Absen::find($id)->update([
                    'keterangan' => 'telat',
                    'perubahan_check_in' => null
                ]);
            }else{
                Absen::find($id)->update([
                    'keterangan' => 'good',
                    'perubahan_check_in' => null
                ]);
            }
        }

        return back()->with('berhasilAcceptPerubahan', 'Data perubahan berhasil disetujui');
    }

    public function declinePerubahan($id)
    {
        $data = Absen::where('id', $id)->get()->toArray();

        $jamMasukShiftPagi = "08:00:00";
        $jamMasukShiftSiang = "14:00:00";
        $jamMasukShiftSore = "16:00:00";

        if($data[0]['shift'] == 'pagi'){
            // dd($data[0]['check_in']);
            if(date("H:i:s", strtotime($data[0]['check_in'])) > $jamMasukShiftPagi){
                Absen::find($id)->update([
                    'keterangan' => 'telat',
                    'perubahan_check_in' => null
                ]);
            }else{
                Absen::find($id)->update([
                    'keterangan' => 'good',
                    'perubahan_check_in' => null
                ]);
            }
        }elseif($data[0]['shift'] == 'sore'){
            if(date("H:i:s", strtotime($data[0]['check_in'])) > $jamMasukShiftSore){
                Absen::find($id)->update([
                    'keterangan' => 'telat',
                    'perubahan_check_in' => null
                ]);
            }else{
                Absen::find($id)->update([
                    'keterangan' => 'good',
                    'perubahan_check_in' => null
                ]);
            }
        }elseif($data[0]['shift'] == 'siang'){
            if(date("H:i:s", strtotime($data[0]['check_in'])) > $jamMasukShiftSiang){
                Absen::find($id)->update([
                    'keterangan' => 'telat',
                    'perubahan_check_in' => null
                ]);
            }else{
                Absen::find($id)->update([
                    'keterangan' => 'good',
                    'perubahan_check_in' => null
                ]);
            }
        }

        return back()->with('declinePerubahan', 'Data perubahan tidak disetujui');
    }

    public function absenExport()
    {
        return Excel::download(new AbsensExport, 'data-users.xlsx');
    }

}
