<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\SlipGaji;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\TunjanganKaryawan;
use Illuminate\Support\Facades\DB;

class SlipGajiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->bulan === null) {
            $bulan = Carbon::now()->format('m') - 1;
        }

        return view('admin.hitung-gaji',[
            'title' => 'Hitung Gaji',
            'bulan' => $bulan
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $all_in = SlipGaji::where('user_id', auth()->user()->id)
        ->where('bulan', $request->bulan)
        ->where('tahun', $request->tahun)
        ->pluck('nominal')->sum();

        $users = User::all();
        SlipGaji::where('bulan', $request->bulan)->where('tahun', $request->tahun)->delete();
        foreach($users as $user){
            $totalTelat = DB::table('absens')->where('user_id', $user->id)
                ->whereMonth('check_in', $request->bulan)
                ->whereYear('check_in', $request->tahun)
                ->where('keterangan', 'telat')->count();
            $totalGood = DB::table('absens')->where('user_id', $user->id)
                ->whereMonth('check_in', $request->bulan)
                ->whereYear('check_in', $request->tahun)
                ->where('keterangan', 'good')->count();
            $tunjangan_karyawans = TunjanganKaryawan::where('user_id', $user->id)->get();
                foreach($tunjangan_karyawans as $tunjangan_karyawan){
                    if($tunjangan_karyawan->tunjangan->pengali_berdasarkan == 'kehadiran'){
                        $nominal = $tunjangan_karyawan->tunjangan->nominal * ($totalTelat+$totalGood);
                    }elseif($tunjangan_karyawan->tunjangan->pengali_berdasarkan == 'gaji_pokok'){
                        $nominal = $tunjangan_karyawan->tunjangan->nominal * $tunjangan_karyawan->tunjangan->persentase * $user->gaji_pokok;
                    }else{
                        $nominal = $tunjangan_karyawan->nominal;
                    }
                //tunjangan-tunjangan
                SlipGaji::create([
                    'user_id' => $user->id,
                    'bulan' => $request->bulan,
                    'tahun' => $request->tahun,
                    'jenis_pendapatan' => $tunjangan_karyawan->jenis_tunjangan,
                    'nominal' => $nominal
                ]);
            }
             //gaji pokok dari tabel user
            SlipGaji::create([
                'user_id' => $user->id,
                'bulan' => $request->bulan,
                'tahun' => $request->tahun,
                'jenis_pendapatan' => 'gaji pokok',
                'nominal' => $user->gaji_pokok
            ]);

            //lembur
            $totalNominalLembur = DB::table('lemburs')->where('user_id', $user->id)
                ->whereMonth('tanggal_lembur', $request->bulan)
                ->whereYear('tanggal_lembur', $request->tahun)
                ->where('approval', 'accept')
                ->get()
                ->sum('nominal_lembur');
                
            SlipGaji::create([
                'user_id' => $user->id,
                'bulan' => $request->bulan,
                'tahun' => $request->tahun,
                'jenis_pendapatan' => 'lembur',
                'nominal' => $totalNominalLembur
            ]);

            //telat
            $totalTelat = DB::table('absens')->where('user_id', $user->id)
                ->whereMonth('check_in', $request->bulan)
                ->whereYear('check_in', $request->tahun)
                ->where('keterangan', 'telat')->count();
            
            SlipGaji::create([
                'user_id' => $user->id,
                'bulan' => $request->bulan,
                'tahun' => $request->tahun,
                'jenis_pendapatan' => 'telat',
                'nominal' => 0 - ($totalTelat * 2500)
            ]);
        }

        return back()->with('selesai_hitung', 'Perhitungan slip gaji selesai dilakukan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SlipGaji  $slipGaji
     * @return \Illuminate\Http\Response
     */
    public function show(SlipGaji $slipGaji)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SlipGaji  $slipGaji
     * @return \Illuminate\Http\Response
     */
    public function edit(SlipGaji $slipGaji)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SlipGaji  $slipGaji
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SlipGaji $slipGaji)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SlipGaji  $slipGaji
     * @return \Illuminate\Http\Response
     */
    public function destroy(SlipGaji $slipGaji)
    {
        //
    }
}
