<?php

namespace App\Http\Controllers;

use App\Models\Cuti;
use App\Models\User;
use App\Models\Absen;
use App\Models\Shift;
use App\Models\Lembur;
use App\Models\Holiday;
use App\Models\SlipGaji;
use App\Models\Tunjangan;
use Facade\FlareClient\View;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\TunjanganKaryawan;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\HolidayController;

class PagesController extends Controller
{
    private $company;
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->company = User::where('id', auth()->user()->id)->first()->company;
    
            return $next($request);
        });
    }

    public function dashboard()
    {
        $notif = Shift::where('approval', 'pending')->get()->count();
        $totalKaryawan = User::all()->count();
        $karyawan = User::all();

        return view('dashboard', [
            'title' => 'Dashboard',
            'notif' => $notif,
            'totalKaryawan' => $totalKaryawan,
            'karyawan' => $karyawan
        ]);
    }

    public function home()
    {
        //pengecekan perubahan shift
        $tanggalShift = DB::table('shifts')->latest()->where('name', auth()->user()->name)
            ->whereDate('tanggal', Carbon::now())
            ->where('approval', 'accept')
            ->pluck('tanggal')->toArray();

        $shift = DB::table('shifts')->latest()->where('name', auth()->user()->name)
            ->whereDate('tanggal', Carbon::now())
            ->where('approval', 'accept')
            ->pluck('shift')->toArray();

        $jam = DB::table('shifts')->latest()->where('name', auth()->user()->name)
            ->whereDate('tanggal', Carbon::now())
            ->where('approval', 'accept')
            ->pluck('jam')->toArray();

        if ($tanggalShift !== []) {
            if (Carbon::today()->format('Y-m-d') == $tanggalShift[0]) {
                User::where('id', auth()->user()->id)->update([
                    'perubahan_shift' => $shift[0],
                    'perubahan_jam' => $jam[0]
                ]);
            }
        } else {
            User::where('id', auth()->user()->id)->update([
                'perubahan_shift' => null,
                'perubahan_jam' => null
            ]);
        }

        if (auth()->user()->perubahan_shift == null) {
            $userShift = auth()->user()->shift;
        } else {
            $userShift = auth()->user()->perubahan_shift;
        }
        if ($userShift == 'pagi') {
            $jamShift = "08:00 - 22:00";
        } elseif ($userShift == 'sore') {
            $jamShift = "16:00 - 22:00";
        } elseif ($userShift == 'siang') {
            $jamShift = "14:00 - 22:00";
        }

        //konfirmasi pengecekan apakah sebelumnya sudah melakukan checkout atau belum
        $cek_checkout = DB::table('absens')->where('user_id', auth()->user()->id)
            ->whereDate('created_at', Carbon::now()->format('Y-m-d'))
            ->pluck('check_out')
            ->toArray();

        $cek_checkout2 = DB::table('absens')->where('user_id', auth()->user()->id)
        ->whereDate('created_at', Carbon::now()->format('Y-m-d'))
        ->where('check_out', $cek_checkout)
        ->get()
        ->count();

        return view('home', [
            'title' => 'Absensi Magang',
            'jamShift' => $jamShift,
            'userShift' => $userShift,
            'cek_checkout' => $cek_checkout,
            'cek_checkout2' => $cek_checkout2
        ]);
    }

    public function docApi()
    {
        return view('doc-api', [
            'title' => 'API Documentation'
        ]);
    }

    public function admin()
    {
        return redirect('/dashboard');
    }

    public function history(Request $request)
    {
        $notif = Shift::where('approval', 'pending')->get()->count();

        if ($request->keyword) {
            if (!$request->bulan) {
                $data = DB::table('absens')
                ->where('company_id', $this->company->id)
                ->where('name', 'like', '%' . $request->keyword . '%')->paginate(25);
            }else{
                $bulan = $request->bulan;
                $data = DB::table('absens')
                    ->where('company_id', $this->company->id)
                    ->where('name', 'like', '%' . $request->keyword . '%')
                    ->whereMonth('check_in', $bulan)
                    ->paginate(25);
            }
        } else {
            $data = Absen::where('company_id', $this->company->id)->latest()->paginate(20)->withQueryString();
        }

        return view('admin.history', [
            'data' => $data->withQueryString(),
            'title' => 'History',
            'notif' => $notif
        ]);
    }

    public function pengaturanShift(Request $request)
    {
        $notif = Shift::where('approval', 'pending')->get()->count();

        if ($request->keyword) {
            $data = DB::table('users')->where('name', 'like', '%' . $request->keyword . '%')->get();
        } else {
            $data = User::all();
        }
        return view('admin.pengaturan-shift', [
            'title' => 'Pengaturan Shift',
            'data' => $data,
            'notif' => $notif
        ]);
    }

    public function pengaturanShiftEdit($id)
    {
        $notif = Shift::where('approval', 'pending')->get()->count();
        $data = User::find($id)->toArray();

        return view('admin.pengaturan-shift-edit', [
            'title' => 'Edit Pengaturan Shift',
            'data' => $data,
            'notif' => $notif
        ]);
    }

    public function requestShift()
    {
        $user = User::find(auth()->user()->id)->toArray();
        $shift = Shift::where('name', auth()->user()->name)->get();

        return view('request-shift', [
            'user' => $user,
            'shift' => $shift
        ]);
    }

    public function listRequestShift()
    {
        $data = Shift::where('approval', 'pending')->get();
        $notif = Shift::where('approval', 'pending')->get()->count();

        return view('admin.list-request-shift', [
            'title' => 'List Request Shift',
            'data' => $data,
            'notif' => $notif
        ]);
    }

    public function listAllRequestShift()
    {
        $shift = Shift::latest()->paginate(20)->withQueryString();
        $notif = Shift::where('approval', 'pending')->get()->count();

        return view('admin.list-all-request-shift', [
            'title' => 'List All Request',
            'notif' => $notif,
            'shift' => $shift
        ]);
    }

    public function holiday()
    {
        $notif = Shift::where('approval', 'pending')->get()->count();
        $holiday = Holiday::all();

        return view('admin.holiday', [
            'title' => 'Holiday',
            'notif' => $notif,
            'holiday' => $holiday
        ]);
    }

    public function holidayEdit($id)
    {
        $notif = Shift::where('approval', 'pending')->get()->count();
        $holiday = Holiday::where('id', $id)->get()->toArray();

        return view('admin.holiday-edit', [
            'title' => 'Holiday',
            'notif' => $notif,
            'holiday' => $holiday
        ]);
    }

    public function attendanceList(Request $request)
    {
        //Jika ada pencarian
        if ($request->bulan && $request->tahun && $request->id) {
            $data = DB::table('absens')->where('user_id', $request->id)
            ->whereMonth('check_in', $request->bulan)
            ->whereYear('check_in', $request->tahun)
            ->get();

            $totalTelat = DB::table('absens')->where('user_id', $request->id)
                ->whereMonth('check_in', $request->bulan)
                ->whereYear('check_in', $request->tahun)
                ->where('keterangan', 'telat')->count();

            $totalGood = DB::table('absens')->where('user_id', $request->id)
                ->whereMonth('check_in', $request->bulan)
                ->whereYear('check_in', $request->tahun)
                ->where('keterangan', 'good')->count();

            $totalCuti = DB::table('absens')->where('user_id', $request->id)
                ->whereMonth('check_in', $request->bulan)
                ->whereYear('check_in', $request->tahun)
                ->where('keterangan', 'cuti')->count();

            $totalHariLembur = DB::table('lemburs')->where('user_id', $request->id)
                ->whereMonth('tanggal_lembur', $request->bulan)
                ->whereYear('tanggal_lembur', $request->tahun)
                ->where('approval', 'accept')
                ->count();

            $totalNominalLembur = DB::table('lemburs')->where('user_id', $request->id)
                ->whereMonth('tanggal_lembur', $request->bulan)
                ->whereYear('tanggal_lembur', $request->tahun)
                ->where('approval', 'accept')
                ->get()
                ->sum('nominal_lembur');

            $perubahanCheckIn = DB::table('absens')->where('user_id', $request->id)
                ->whereMonth('check_in', $request->bulan)
                ->whereYear('check_in', $request->tahun)
                ->pluck('perubahan_check_in')->whereNotNull()->count();

            $jumlahMasukKerja = $totalTelat + $totalGood;

            $slip_gaji = DB::table('slip_gajis')->where('user_id', $request->id)
                    ->where('bulan', $request->bulan)
                    ->where('tahun', $request->tahun)
                    ->get();

            if ($request->bulan === null) {
                $bulan = Carbon::now()->format('m');
            }

            $bulan = $request->bulan;
        } else {
            $data = DB::table('absens')->where('user_id', auth()->user()->id)
                ->whereMonth('check_in', Carbon::today())
                ->whereYear('check_in', Carbon::today())
                ->get();

            $totalTelat = DB::table('absens')->where('user_id', auth()->user()->id)
                ->whereMonth('check_in', Carbon::today())
                ->whereYear('check_in', Carbon::today())
                ->where('keterangan', 'telat')
                ->count();

            $totalGood = DB::table('absens')->where('user_id', auth()->user()->id)
                ->whereMonth('check_in', Carbon::today())
                ->whereYear('check_in', Carbon::today())
                ->where('keterangan', 'good')
                ->count();

            $totalCuti = DB::table('absens')->where('user_id', auth()->user()->id)
                ->whereMonth('check_in', Carbon::today())
                ->whereYear('check_in', Carbon::today())
                ->where('keterangan', 'cuti')
                ->count();
                
            $totalHariLembur = DB::table('lemburs')->where('user_id', auth()->user()->id)
                ->whereMonth('tanggal_lembur', Carbon::today())
                ->whereYear('tanggal_lembur', Carbon::today())
                ->where('approval', 'accept')
                ->count();

            $totalNominalLembur = DB::table('lemburs')->where('user_id', auth()->user()->id)
                ->whereMonth('tanggal_lembur', Carbon::today())
                ->whereYear('tanggal_lembur', Carbon::today())
                ->where('approval', 'accept')
                ->get()
                ->sum('nominal_lembur');

            $perubahanCheckIn = DB::table('absens')->where('user_id', auth()->user()->id)
                ->whereMonth('check_in', Carbon::today())
                ->whereYear('check_in', Carbon::today())
                ->pluck('perubahan_check_in')->whereNotNull()
                ->count();
            
            $jumlahMasukKerja = $totalTelat + $totalGood + $totalHariLembur;

            $slip_gaji = DB::table('slip_gajis')->where('user_id', auth()->user()->id)
                    ->where('bulan', Carbon::now()->format('m')-1)
                    ->where('tahun', Carbon::now()->format('Y'))
                    ->get();
            
            $jenis_tunjangan = TunjanganKaryawan::where('user_id', auth()->user()->id)->get()->pluck('jenis_tunjangan')->toArray();
            $jumlah_tunjangan = TunjanganKaryawan::where('user_id', auth()->user()->id)->get()->count();
            $total_tunjangan = 0;
            
            if($jumlah_tunjangan > 0){
                for($i=0; $i < $jumlah_tunjangan; $i++){
                    if($jenis_tunjangan[$i] == 'Makan'){
                        $nominal = TunjanganKaryawan::where('user_id', auth()->user()->id)->get()
                            ->where('jenis_tunjangan', $jenis_tunjangan[$i])
                            ->pluck('nominal')->toArray();
                        $total_tunjangan += $nominal[0] * $jumlahMasukKerja;
                        $totalTunjanganMakan = $nominal[0] * $jumlahMasukKerja;
                    }elseif($jenis_tunjangan[$i] == 'Transport'){
                        $nominal = TunjanganKaryawan::where('user_id', auth()->user()->id)->get()
                            ->where('jenis_tunjangan', $jenis_tunjangan[$i])
                            ->pluck('nominal')->toArray();
                        $total_tunjangan += $nominal[0] * $jumlahMasukKerja;
                        $totalTunjanganTransport = $nominal[0] * $jumlahMasukKerja;
                    }else{
                        $nominal = TunjanganKaryawan::where('user_id', auth()->user()->id)->get()
                            ->where('jenis_tunjangan', $jenis_tunjangan[$i])
                            ->pluck('nominal')->toArray();
                            $total_tunjangan += $nominal[0];
                    }
                }
            }

            if ($request->bulan === null) {
                $bulan = Carbon::now()->format('m');
            }
        }

        // dd($jenisTunjangan);
        $userAll = User::where('company_id', $this->company->id)->get();
        $notif = Shift::where('approval', 'pending')->get()->count();
        $lembur = Lembur::where('user_id', $request->id)
            ->where('approval','!=' ,'decline')
            ->get();

        return view('attendance-list', [
            'title' => 'Attendance List',
            'notif' => $notif,
            'data' => $data,
            'bulan' => $bulan,
            'jumlahMasukKerja' => $jumlahMasukKerja,
            'totalTelat' => $totalTelat,
            'totalGood' => $totalGood,
            'totalCuti' => $totalCuti,
            'perubahan_check_in' => $perubahanCheckIn,
            'totalNominalLembur' => $totalNominalLembur,
            'totalHariLembur' => $totalHariLembur,
            'totalTunjanganMakan' => 0,
            'lembur' => $lembur,
            'user' => $userAll,
            'slip_gaji' => $slip_gaji,
            'company' => $this->company
        ]);
    }

    public function cuti()
    {
        $notif = Shift::where('approval', 'pending')->get()->count();
        $jatahCuti = User::where('id', auth()->user()->id)->pluck('jatah_cuti')->toArray();

        return view('request-cuti', [
            'title' => 'Pengajuan Cuti',
            'notif' => $notif,
            'jatah_cuti' => $jatahCuti[0]
        ]);
    }

    public function listRequestCuti()
    {
        $notif = Shift::where('approval', 'pending')->get()->count();
        $data = Cuti::where('approval', 'pending')->get();

        return view('admin.list-request-cuti', [
            'title' => 'List Request Cuti',
            'notif' => $notif,
            'data' => $data
        ]);
    }

    public function listRequestCutiAll()
    {
        $notif = Shift::where('approval', 'pending')->get()->count();
        $data  = Cuti::all();

        return view('admin.list-all-request-cuti', [
            'title' => 'List All Request Cuti',
            'notif' => $notif,
            'data' => $data
        ]);
    }

    public function profile($id)
    {
        $notif = Shift::where('approval', 'pending')->get()->count();
        $data  = User::where('id', $id)->get()->toArray();

        return view('profile', [
            'title' => 'Profile',
            'notif' => $notif,
            'data' => $data
        ]);
    }

    public function pengajuanPerubahanAbsen($id)
    {
        $notif = Shift::where('approval', 'pending')->get()->count();
        $data  = Absen::where('id', $id)->get()->toArray();
        $tanggal = explode(" ", $data[0]['check_in']);

        return view('pengajuan-perubahan-absen', [
            'title' => 'Pengajuan Perubahan Absen',
            'notif' => $notif,
            'data' => $data,
            'tanggal' => $tanggal[0]
        ]);
    }

    public function requestKoreksiKehadiran()
    {
        $notif = Shift::where('approval', 'pending')->get()->count();
        $data = Absen::where('keterangan', 'koreksi kehadiran pending')->get();

        return view('request-koreksi-kehadiran', [
            'title' => 'Request Koreksi Kehadiran',
            'notif' => $notif,
            'data' => $data
        ]);
    }

    public function setting()
    {
        return view('admin.setting', [
            'title' => 'Setting'
        ]);
    }

    public function pengaturanTunjangan()
    {
        $data = Tunjangan::all();

        return view('admin.pengaturan-tunjangan', [
            'title' => 'Pengaturan Tunjangan',
            'data' => $data
        ]);
    }

    public function pengaturanTunjanganKaryawan(Request $request)
    {
        $user = User::all();
        $tunjangan = Tunjangan::all();
        $tunjanganKaryawan = TunjanganKaryawan::where('user_id', auth()->user()->id)->get();
        
        if($request->id){
            $tunjanganKaryawan = TunjanganKaryawan::where('user_id', $request->id)->get();
        }

        return view('admin.pengaturan-tunjangan-karyawan', [
            'title' => 'Pengaturan Tunjangan Karyawan',
            'user' => $user,
            'tunjangan' => $tunjangan,
            'tunjanganKaryawan' => $tunjanganKaryawan
        ]);
    }

    public function requestLembur()
    {
        $notif = Shift::where('approval', 'pending')->get()->count();

        return view('admin.request-lembur',[
            'title' => 'Request Lembur',
            'notif' => $notif
        ]);
    }

    public function listRequestLembur()
    {
        $notif = Shift::where('approval', 'pending')->get()->count();
        $data = Lembur::where('approval', 'pending')->get();

        return view('admin.list-request-lembur',[
            'title' => 'List Overtime Request',
            'notif' => $notif,
            'data' => $data
        ]);
    }

    public function listAllRequestLembur()
    {
        $notif = Shift::where('approval', 'pending')->get()->count();
        $data = Lembur::all();

        return view('admin.list-all-request-lembur',[
            'title' => 'List Overtime Request',
            'notif' => $notif,
            'data' => $data
        ]);
    }

    public function pengaturanTunjanganBerdasarkanTunjangan(Request $request)
    {
        $notif = Shift::where('approval', 'pending')->get()->count();
        $jenis_tunjangan = Tunjangan::all();
        $user = User::all();
        $tunjangan_karyawan = TunjanganKaryawan::where('tunjangan_id', $request->tunjangan_id)->get();
        // dd($tunjangan_karyawan);

        return view('admin.pengaturan-tunjangan-berdasarkan-tunjangan',[
            'title' => 'Pengaturan Tunjangan Berdasarkan Tunjangan',
            'notif' => $notif,
            'jenis_tunjangan' => $jenis_tunjangan,
            'user' => $user,
            'tunjangan_karyawan' => $tunjangan_karyawan
        ]);
    }
}
