<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginApiController extends Controller
{
    public function loginUser(Request $request)
    {
        //params
        $key = $request->key;
        $email = $request->email;
        $password = $request->password;

        try{
            $verifiedEmail = User::where('email', $email)->pluck('email_verified_at');
            $id = User::where('email', $email)->pluck('id');

            if($verifiedEmail[0] !== null){
                if($key == 'AbsensiMagang' && Auth::attempt(['email' => $email, 'password' => $password ])){
                    return response()->json([
                        'status' => 'Success',
                        'message' => 'Berhasil login',
                        'id' => $id[0]
                    ], 200);
                }else{
                   return response()->json([
                       'status' => 'Error',
                       'message' => 'Login gagal'
                   ], 401);
                }
            }else{
                return response()->json([
                    'status' => 'Error',
                    'message' => 'Email address is not verified'
                ], 554);
            }
        }catch(\Exception $exception){
            return response()->json([
                'status' => 'Error',
                'message' => 'System Error'
            ], 401);
        }
    }
}
