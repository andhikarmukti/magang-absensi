<?php

namespace App\Http\Controllers;

use App\Models\Shift;
use Illuminate\Http\Request;

class ShiftApiController extends Controller
{
    public function store(Request $request)
    {
        $user = $request->user;
        $key = $request->key;
        $id = $request->id;
        $name = $request->name;
        $tanggal = $request->tanggal;
        $shift = $request->shift;
        $jam = $request->jam;
        $entitas = $request->entitas;
        $keterangan = $request->keterangan;

        if($user == 'admin' && $key == 'AbsensiMagang' && $name !== null && $tanggal !== null && $shift !== null && $entitas !== null){
            Shift::create([
                'user_id' => $id,
                'name' => $name,
                'tanggal' => $tanggal,
                'shift' => $shift,
                'jam' => $jam,
                'entitas' => $entitas,
                'keterangan' => $keterangan
            ]);

            return response()->json([
                'status' => 'Success',
                'message' => 'Berhasil Melakukan Pengajuan Perubahan Shift'
            ], 200);
        }else{
            return response()->json([
                'status' => 'Error',
                'message' => 'Bad Auth'
            ], 401);
        }
    }
}
