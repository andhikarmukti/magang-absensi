<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Exports\UsersExport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    public function pengaturanShiftUpdate(Request $request)
    {

        User::where('id', $request->id)->update([
            'entitas' => $request->entitas,
            'shift' => $request->shift
        ]);
        
        return redirect('/setting/pengaturan-shift')->with('berhasilDiubah', 'Data berhasil diubah');
    }

    public function profile(Request $request)
    {   
        if($request->profile_image ==  null){
            return back()->with('gambarKosong', 'Anda belum memlilih file untuk diupload');
        }

        if($request->profile_image_lama !== 'img/default.jpg'){
            Storage::delete($request->profile_image_lama);
        }

        $request->validate([
            'profile_image' => 'required|image|file|max:1024'
        ]);

        User::where('id', auth()->user()->id)->update([
            'profile_image' => $request->file('profile_image')->store('img')
        ]);

        return back()->with('berhasilUbahGambar', 'Profile picture berhasil diubah');
    }

    public function userExport()
    {
        return Excel::download(new UsersExport, 'data-users.xlsx');
    }
}
