<?php

namespace App\Http\Controllers;

use App\Models\Cuti;
use App\Models\User;
use Illuminate\Http\Request;

class CutiController extends Controller
{
    public function store(Request $request)
    {
        $selisihHariCuti = strtotime($request->hingga_tanggal) - strtotime($request->dari_tanggal);
        $totalHariCuti = abs($selisihHariCuti / (365 * 60 * 60 * 24) * 365) + 1;
        $jatahCuti = User::where('id', auth()->user()->id)->pluck('jatah_cuti')->toArray();
        if($jatahCuti[0] - $totalHariCuti < 0){
            return back()->with('tidakBisaCuti', 'Maaf, jatah cuti anda tidak mencukupi untuk pengambilan cuti selama ' . round($totalHariCuti, 0) . ' hari');
        };

        $request->validate([
            'dari_tanggal' => 'required',
            'hingga_tanggal' => 'required',
            'jenis_cuti' => 'required',
            'keterangan' => 'required',
        ]);
        
        Cuti::create([
            'user_id' => $request->user_id,
            'name' => $request->name,
            'dari_tanggal' => $request->dari_tanggal,
            'hingga_tanggal' => $request->hingga_tanggal,
            'jenis_cuti' => $request->jenis_cuti,
            'keterangan' => $request->keterangan,
        ]);

        return back()->with('berhasilSubmitCuti', 'Pengajuan cuti berhasil terkirim, silahkan tunggu konfirmasi');
    }

    public function acceptRequestCuti($id)
    {
        $jatahCuti = User::where('id', auth()->user()->id)->pluck('jatah_cuti')->toArray();
        
        $Cuti = Cuti::where('id', auth()->user()->id)->get()->toArray();
        $selisihHariCuti = strtotime($Cuti[0]['hingga_tanggal']) - strtotime($Cuti[0]['dari_tanggal']);
        $totalHariCuti = abs($selisihHariCuti / (365 * 60 * 60 * 24) * 365) + 1;
        $sisaCuti = $jatahCuti[0] - $totalHariCuti;

        Cuti::where('id', $id)->update([
            'approval' => 'accept',
        ]);

        User::where('id', auth()->user()->id)->update([
            'jatah_cuti' => $sisaCuti
        ]);

        return back()->with('acceptRequestCuti', 'Request cuti berhasil disetujui');
    }

    public function declineRequestCuti($id)
    {
        Cuti::where('id', $id)->update([
            'approval' => 'decline'
        ]);

        return back()->with('declineRequestCuti', 'Request cuti tidak disetujui');
    }
}
