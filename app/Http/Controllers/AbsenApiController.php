<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\User;
use App\Models\Absen;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class AbsenApiController extends Controller
{
    public function allDataAbsen(Request $request)
    {
        $user = $request->user;
        $key = $request->key;

        if($user == 'admin' && $key == 'AbsensiMagang'){
            $data = Absen::all();

            return response()->json([
                'status' => 'Success',
                'data' => $data
            ], 200);
        }else{
            return response()->json([
                'status' => 'Error',
                'message' => 'Bad Auth'
            ], 401);
        }
    }

    public function absen(Request $request)
    {
        //key
        $id = $request->id;
        $user = $request->user;
        $key = $request->key;

        //pengecekan key dan value
        if ($user == 'admin' && $key == 'AbsensiMagang') {
            $data = Absen::latest()->where('user_id', $id)->get();
        } else {
            $data = null;
        }

        if ($data == null) {
            return response()->json([
                'status' => 'error',
                'message' => 'bad auth'
            ], 401);
        } else {
            return response()->json([
                'status' => 'Success',
                'data' => $data
            ], 200);
        }
    }

    public function store(Request $request)
    {
        $user = $request->user;
        $key = $request->key;
        $image = $request->image;
        $shift = $request->shift;
        $id = $request->id;
        $perubahan_jam = $request->perubahan_jam;
        $name = User::where('id', $id)->get()->pluck('name')->toArray();
        $latitude2 = $request->latitude;
        $longitude2 = $request->longitude;

        if ($user == 'admin' && $key == 'AbsensiMagang' && $image !== null && $id !== null && $latitude2!== null && $longitude2!== null) {
            //pengecekan lokasi
            $latitude1 = "-6.026194";
            $longitude1 =  "106.046944";
            $earth_radius = 6371; // earth radius in km

            $dLat = deg2rad($latitude2 - $latitude1);
            $dLon = deg2rad($longitude2 - $longitude1);

            $a = sin($dLat/2) * sin($dLat/2) + cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * sin($dLon/2) * sin($dLon/2);
            $c = 2 * asin(sqrt($a));
            $distance = $earth_radius * $c;
            $distance = $distance * 1000; // satuan meter

            //pengecekan kondisi jika jarak terlalu jauh
            if($distance > 20){
                return response()->json([
                    'status' => 'Error',
                    'message' => 'Jarak terlalu jauh dari batas jangkauan',
                    'distance' => $distance,
                    'a' => $a,
                    'c' => $c,
                    'dLat' => $dLat,
                    'dLon' => $dLon,
                    'latitude2' => $latitude2,
                    'longitude2' => $longitude2,
                ], 200);
            }

            //cek sudah absen atau belum?
            $today = Carbon::now()->format('Y-m-d');
            $cekAbsenToday = DB::table('absens')->where('user_id', $id)
                ->whereDate('created_at', $today)
                ->count();

            //cek apakah sudah upload foto checkin
            $cekFotoCheckin = DB::table('absens')->where('user_id', $id)
                ->whereDate('created_at', $today)
                ->pluck('check_in_image');
            // dd($cekFotoCheckin);

            // upload data base64
            $img = $request->image;
            $folderPath = "storage/img/"; 
            $image_parts = explode(";base64,", $img);
            $image_type_aux = explode("image/", $image_parts[0]);
            $image_type = $image_type_aux[1]; 
            $image_base64 = base64_decode($image_parts[1]);
            $fileName = uniqid() . '.png';
            $file = $folderPath . $fileName;
            file_put_contents($file, $image_base64);

            //menghitung selisih waktu
            if (
                DB::table('absens')->where('user_id', $id)
                ->whereDate('created_at', $today)
                ->pluck('check_in')->count() > 0
            ) {
                DB::table('absens')->where('user_id', $id)
                    ->whereDate('created_at', $today)
                    ->update([
                        'check_out' => Carbon::now()
                    ]);
                $checkin = DB::table('absens')->where('user_id', $id)
                    ->whereDate('created_at', $today)
                    ->pluck('check_in');
                $checkout = DB::table('absens')->where('user_id', $id)
                    ->whereDate('created_at', $today)
                    ->pluck('check_out');
                $selisihJamKerja = strtotime($checkout[0]) - strtotime($checkin[0]);
                $totalJamKerja = gmdate("H:i:s", $selisihJamKerja);
            }
            
            //pengecekan sudah ada submit di hari ini atau belum
            if ($cekAbsenToday == 0) {
                Absen::create([
                    'name' => $name[0],
                    'check_in' => Carbon::now(),
                    'jam_check_in' => date_format(date_create(Carbon::now()), "H:i:s"),
                    'user_id' => $id,
                    'check_in_image' => $fileName
                ]);
            } else {
                //menghapus foto checkout yang lama sebelum mengganti dengan yang baru
                $fotoCheckOut = DB::table('absens')->where('user_id', $id)
                    ->whereDate('created_at', $today)
                    ->pluck('check_out_image');

                if($fotoCheckOut[0] != 'default.jpg'){
                    Storage::delete('img/' . $fotoCheckOut[0]);
                };

                //jika sudah ada submit, maka submit berikutnya akan jadi checkout
                DB::table('absens')->where('user_id', $id)
                    ->whereDate('created_at', $today)
                    ->update([
                        'check_out' => Carbon::now(),
                        'jam_check_out' => date_format(date_create(Carbon::now()), "H:i:s"),
                        'check_out_image' => $fileName,
                        'selisih_waktu' => $totalJamKerja
                    ]);
                return response()->json([
                    'status' => 'Success',
                    'message' => ' Berhasil melakukan check out',
                    // 'distance' => $distance,
                    // 'a' => $a,
                    // 'c' => $c,
                    // 'dLat' => $dLat,
                    // 'dLon' => $dLon,
                    // 'latitude2' => $latitude2,
                    // 'longitude2' => $longitude2,
                    // 'base64' => $request->image,
                ], 200);
            }

            //update keterangan telat atau good berdasarkan data checkin yang sudah masuk ke db
            //cek apakah ada perubahan jam kerja
            if($perubahan_jam == null){
                $jamMasukShiftPagi = "08:00:00";
                $jamMasukShiftSiang = "14:00:00";
                $jamMasukShiftSore = "16:00:00";
            }else{
                $jamKerja = $perubahan_jam;
                $jamMasukShiftPagi = $jamKerja;
                $jamMasukShiftSiang = $jamKerja;
                $jamMasukShiftSore = $jamKerja;
            }
            $jamMasukShiftPagi = "08:00:00";
            $jamMasukShiftSore = "16:00:00";
            $data = DB::table('absens')->where('user_id', $id)
            ->whereDate('created_at', $today)
            ->get()->all();

            if($shift == 'pagi'){
                if(date("H:i:s", strtotime($data[0]->check_in)) > $jamMasukShiftPagi){
                DB::table('absens')->where('user_id', $id)
                ->whereDate('created_at', $today)
                ->update([
                        'keterangan' => 'telat',
                        'shift' => $shift
                    ]);
                }else{
                DB::table('absens')->where('user_id', $id)
                ->whereDate('created_at', $today)
                ->update([
                        'keterangan' => 'good',
                        'shift' => $shift
                    ]);
                }
            }else{
                if(date("H:i:s", strtotime($data[0]->check_in)) > $jamMasukShiftSore){
                DB::table('absens')->where('user_id', $id)
                ->whereDate('created_at', $today)
                ->update([
                        'keterangan' => 'telat',
                        'shift' => $shift
                    ]);
                }else{
                DB::table('absens')->where('user_id', $id)
                ->whereDate('created_at', $today)
                ->update([
                        'keterangan' => 'good',
                        'shift' => $shift
                    ]);
                }
            }

            return response()->json([
                'status' => 'Success',
                'message' => ' Berhasil melakukan check in'
            ], 200);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'bad auth'
            ], 401);
        }
    }

    public function destroy(Request $request)
    {
        $user = $request->user;
        $key = $request->key;
        try{
            if($user == 'admin' && $key == 'AbsensiMagang'){
                if(Absen::where('id', $request->id)->count() > 0){
                    $name = Absen::where('id', $request->id)->pluck('name');
                    $check_in = Absen::where('id', $request->id)->pluck('check_in');
                    $check_out = Absen::where('id', $request->id)->pluck('check_out');
                    Absen::destroy($request->id);
                    return response()->json([
                        'status' => 'Success',
                        'data' => [
                            'id' => $request->id,
                            'name' => $name[0],
                            'check_in' => $check_in[0],
                            'check_out' => $check_out[0]
                        ],
                        'message' => 'Data berhasil dihapus',
                    ], 200);
                }else{
                    return response()->json([
                        'status' => 'Error',
                        'message' => 'Data gagal dihapus'
                    ], 500);
                };
            }else{
                return response()->json([
                    'status' => 'Error',
                    'message' => 'Bad Auth'
                ], 401);
            };
        }catch(\Exception $exception){
            return response()->json([
                'status' => 'Error',
                'message' => 'Data gagal dihapus'
            ], 500);
        }
    }

    public function jumlahMasukKerja(Request $request)
    {
        $user = $request->user;
        $key = $request->key;
        $user_id = $request->user_id;
        $bulan = $request->bulan;
        $tahun = $request->tahun;

        if($user == 'admin' && $key == 'AbsensiMagang' && $user_id !== null && $bulan !== null && $tahun !== null){
            $jumlah_masuk_kerja = DB::table('absens')->where('user_id', $request->user_id)
            ->whereMonth('check_in', $request->bulan)
            ->whereYear('check_in', $request->tahun)
            ->get()->count();

            return response()->json([
                'status' => 'Success',
                'message' => 'Data jumlah masuk kerja',
                'jumlah_masuk_kerja' => $jumlah_masuk_kerja
            ], 200);
        }else{
            return response()->json([
                'status' => 'Error',
                'message' => 'Bad Auth'
            ], 401);
        }
    }

    public function perubahanAbsensi(Request $request)
    {
        $user = $request->user;
        $key = $request->key;
        $tanggal = $request->tanggal;
        $jam = $request->jam;
        $id_absen = $request->id_absen;
        
        try{
            if($user == 'admin' && $key == 'AbsensiMagang' && $id_absen !== null && $jam !== null && $tanggal !== null){
                $keterangan = 'koreksi kehadiran pending';
                $check_in = $request->tanggal . ' ' . $request->jam;
                // dd($id_absen);
                Absen::where('id' ,$request->id_absen)->update([
                    'keterangan' => $keterangan,
                    'perubahan_check_in' => $check_in
                ]);
                
                return response()->json([
                    'status' => 'Success',
                    'message' => 'Berhasil mengajukan perubahan absensi'
                ], 200);
            }else{
                return response()->json([
                    'status' => 'Error',
                    'message' => 'Bad Auth'
                ], 401);
            }
        }catch(\Exception $exception){
            return 'something wrong';
        }
        
    }

}
