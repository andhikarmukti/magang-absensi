<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Tunjangan;
use Illuminate\Http\Request;
use App\Models\TunjanganKaryawan;

class TunjanganKaryawanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        if($request->tunjangan_id){
            $user_name = User::all()->pluck('name');
            TunjanganKaryawan::where('tunjangan_id', $request->tunjangan_id)->delete();
            foreach($user_name as $u_name){
                $u_name = str_replace(' ', '_',$u_name);
                $user = User::where('id', $request->$u_name)->get()->toArray();
                if($user != []){
                    $tunjangan = Tunjangan::where('id', $request->tunjangan_id)->get()->toArray();
                    TunjanganKaryawan::create([
                        'user_id' => $user[0]['id'],
                        'tunjangan_id' => $request->tunjangan_id,
                        'name' => $user[0]['name'],
                        'jenis_tunjangan' => $tunjangan[0]['jenis_tunjangan'],
                        'nominal' => $tunjangan[0]['nominal']
                    ]);
                }
            }
            return back()->with('berhasil', 'Berhasil melakukan perubahan');
        }

        $jenis_tunjangan = Tunjangan::all()->pluck('jenis_tunjangan');
        TunjanganKaryawan::where('user_id', $request->id)->delete();
        foreach($jenis_tunjangan as $jt){
            $jt = str_replace(' ','_',$jt);
            $tunjangan = Tunjangan::where('id', $request->$jt)->get()->toArray();
            if($tunjangan != []){
                $jenisTunjanganKaryawan = TunjanganKaryawan::where('user_id', $request->id)->where('jenis_tunjangan',$tunjangan[0]['jenis_tunjangan'])->get()->toArray();
                if($jenisTunjanganKaryawan == []){
                    $name = User::where('id', $request->id)->get()->toArray();
                    TunjanganKaryawan::create([
                        'user_id' => $request->id,
                        'tunjangan_id' => $tunjangan[0]['id'],
                        'name' => $name[0]['name'],
                        'jenis_tunjangan' => $tunjangan[0]['jenis_tunjangan'],
                        'nominal' => $tunjangan[0]['nominal']
                    ]);
                }
            }
        }
        // $user = User::where('id', auth()->user()->id)->get()->toArray();
        return back()->with('berhasil', 'Berhasil melakukan perubahan');

        // return back()->with('tidakAdaPenambahanTunjangan', 'Tidak ada tunjangan baru yang ditambahkan');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TunjanganKaryawan  $tunjanganKaryawan
     * @return \Illuminate\Http\Response
     */
    public function show(TunjanganKaryawan $tunjanganKaryawan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TunjanganKaryawan  $tunjanganKaryawan
     * @return \Illuminate\Http\Response
     */
    public function edit(TunjanganKaryawan $tunjanganKaryawan)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TunjanganKaryawan  $tunjanganKaryawan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TunjanganKaryawan $tunjanganKaryawan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TunjanganKaryawan  $tunjanganKaryawan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = TunjanganKaryawan::where('id', $id)->get()->toArray();

        TunjanganKaryawan::destroy($id);

        return back()->with('berhasilDelete', 'Tunjangan ' . $user[0]['jenis_tunjangan'] . ' dari user ' . $user[0]['name'] . ' berhasil dihapus');
    }
}
