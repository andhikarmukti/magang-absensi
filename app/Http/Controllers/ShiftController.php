<?php

namespace App\Http\Controllers;

use App\Models\Shift;
use Illuminate\Http\Request;

class ShiftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        Shift::create([
            'user_id' => auth()->user()->id,
            'name' => auth()->user()->name,
            'entitas' => auth()->user()->entitas,
            'tanggal' => $request->tanggal,
            'shift' => $request->shift,
            'jam' => $request->jam,
            'keterangan' => $request->keterangan,
        ]);

        return back()->with('berhasilTambahDataShift', 'Pengajuan pergantian shift telah berhasil terkirim');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function show(Shift $shift)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function edit(Shift $shift)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shift $shift)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Shift  $shift
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shift $shift)
    {
        //
    }

    public function acceptRequest($id)
    {
        Shift::where('id', $id)->update([
            'approval' => 'accept'
        ]);

        return back()->with('accept', 'Perubahan shift telah disetujui');
    }
    
    public function declineRequest($id)
    {
        Shift::where('id', $id)->update([
            'approval' => 'decline'
        ]);

        return back()->with('decline', 'Perubahan shift ditolak');
    }

}
