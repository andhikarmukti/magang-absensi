<?php

namespace App\Http\Controllers;

use App\Models\Tunjangan;
use App\Models\TunjanganKaryawan;
use Illuminate\Http\Request;

class TunjanganApiController extends Controller
{
    public function tunjanganPerUser(Request $request)
    {
        $user = 'admin';
        $key = 'AbsensiMagang';
        $user_id = $request->user_id;

        if($user == 'admin' && $key == 'AbsensiMagang' && $user_id !== null){
            $jenis_tunjangan = TunjanganKaryawan::where('user_id', $user_id)->get()->pluck('jenis_tunjangan');
            // dd($jenis_tunjangan);

            return response()->json([
                'status' => 'Success',
                'message' => 'Data tunjangan karyawan',
                'jenis_tunjangan' => $jenis_tunjangan
            ], 200);
        }else{
            return response()->json([
                'status' => 'Error',
                'message' => 'Bad Auth',
            ], 401);
        }


    }

    public function nominalTunjangan(Request $request)
    {
        $user = 'admin';
        $key = 'AbsensiMagang';
        $jenis_tunjangan = $request->jenis_tunjangan;

        if($user == 'admin' && $key == 'AbsensiMagang' && $jenis_tunjangan !== null){
            $nominal_tunjangan = TunjanganKaryawan::where('jenis_tunjangan', $jenis_tunjangan)
            ->get()->pluck('nominal');
            // dd($nominal_tunjangan);

            return response()->json([
                'status' => 'Success',
                'message' => 'Data nominal tunjangan',
                'nominal_tunjangan' => $nominal_tunjangan[0]
            ], 200);
        }else{
            return response()->json([
                'status' => 'Error'
            ], 401);
        }


    }

}
