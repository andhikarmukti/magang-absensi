<?php

namespace App\Http\Controllers;

use App\Models\Cuti;
use App\Models\User;
use Illuminate\Http\Request;

class CutiApiController extends Controller
{
    public function store(Request $request)
    {   
        $user = $request->user;
        $key = $request->key;
        $dari_tanggal = $request->dari_tanggal;
        $hingga_tanggal = $request->hingga_tanggal;
        $id = $request->id;
        $keterangan = $request->keterangan;
        $jenis_cuti = $request->jenis_cuti;

        if($user == 'admin' && $key == 'AbsensiMagang' && $dari_tanggal !== null && $hingga_tanggal !== null && $id !== null && $keterangan!== null && $jenis_cuti !== null){
            $name = User::where('id', $id)->pluck('name')->toArray();
            $selisihHariCuti = strtotime($hingga_tanggal) - strtotime($dari_tanggal);
            $totalHariCuti = abs($selisihHariCuti / (365 * 60 * 60 * 24) * 365) + 1;
            $jatahCuti = User::where('id', $id)->pluck('jatah_cuti')->toArray();
            if($jatahCuti[0] - $totalHariCuti < 0){
                return response()->json([
                    'status' => 'Error',
                    'message' => 'Maaf, jatah cuti anda tidak mencukupi untuk pengambilan cuti selama ' . round($totalHariCuti, 0) . ' hari'
                ], 401);
            };
            
            Cuti::create([
                'user_id' => $request->id,
                'name' => $name[0],
                'dari_tanggal' => $request->dari_tanggal,
                'hingga_tanggal' => $request->hingga_tanggal,
                'jenis_cuti' => $request->jenis_cuti,
                'keterangan' => $request->keterangan,
            ]);
    
            return response()->json([
                'status' => 'Success',
                'message' => 'Pengajuan cuti berhasil'
            ], 200);
        }else{
            return response()->json([
                'status' => 'Error',
                'message' => 'Bad Auth'
            ], 401);
        }
        
    }
}
