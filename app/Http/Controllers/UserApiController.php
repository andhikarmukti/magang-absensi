<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Absen;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class UserApiController extends Controller
{
    public function user(Request $request)
    {
        $user = $request->user;
        $key = $request->key;
        $id = $request->id;
        // $name = User::where('id', $id)->pluck('name');

        //melakukan update perubahan_shift dan perubahan_jam pada tabel users
        $tanggalShift = DB::table('shifts')->latest()->where('user_id', $id)
        ->whereDate('tanggal', Carbon::now())
        ->where('approval', 'accept')
        ->pluck('tanggal')->toArray();

        $shift = DB::table('shifts')->latest()->where('user_id', $id)
        ->whereDate('tanggal', Carbon::now())
        ->where('approval', 'accept')
        ->pluck('shift')->toArray();
        
        $jam = DB::table('shifts')->latest()->where('user_id', $id)
        ->whereDate('tanggal', Carbon::now())
        ->where('approval', 'accept')
        ->pluck('jam')->toArray();

        if($tanggalShift){
            if(Carbon::today()->format('Y-m-d') == $tanggalShift[0]){
                User::where('id', $id)->update([
                    'perubahan_shift' => $shift[0],
                    'perubahan_jam' => $jam[0]
                ]);
            }else{
                User::where('id', $id)->update([
                    'perubahan_shift' => null,
                    'perubahan_jam' => null
                ]);
            }
        }

        // //pengecekan libur nasional||
        // $tanggalHoliday = DB::table('holidays')->whereDate('holiday_date', Carbon::today()->format('Y-m-d'))->get()->toArray();
        // $jumlahUser = User::all();
        // $users = $jumlahUser->toArray();
        // $cekSudahAbsenHariIni = DB::table('absens')->whereDate('created_at', Carbon::today()->format('Y-m-d'))->get()->toArray();
        // // dd(!$cekSudahAbsenHariIni);
        
        // if($tanggalHoliday){
        //     if(!$cekSudahAbsenHariIni){
        //         for($i=0; $i<$jumlahUser->count(); $i++){
        //             Absen::create([
        //                 'name' => $users[$i]['name'],
        //                 'shift' => 'LIBUR NASIONAL',
        //                 'check_in' => 'LIBUR NASIONAL',
        //                 'check_out' => 'LIBUR NASIONAL',
        //                 'selisih_waktu' => null,
        //                 'check_in_image' => 'LIBUR NASIONAL',
        //                 'check_out_image' => 'LIBUR NASIONAL',
        //                 'keterangan' => 'LIBUR NASIONAL'
        //             ]);
        //         }
        //     }
        // }

        if($user == 'admin' && $key == 'AbsensiMagang'){
            $data = User::where('id', $id)->get()->toArray();

            return response()->json([
                'id' => $data[0]['id'],
                'name' => $data[0]['name'],
                'entitas' => $data[0]['entitas'],
                'shift' => $data[0]['shift'],
                'perubahan_shift' => $data[0]['perubahan_shift'],
                'perubahan_jam' => $data[0]['perubahan_jam'],
                'profile_image' => $data[0]['profile_image'],
                'jatah_cuti' => $data[0]['jatah_cuti'],
            ], 200);
        }else{
            return response()->json([
                'status' => 'Error',
                'message' => 'Bad Auth'
            ], 401);
        }
    }
}
