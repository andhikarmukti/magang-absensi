<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;

class RegisterApiController extends Controller
{
    public function registerUser(Request $request)
    {
        // params
        $key = $request->key;
        $name = $request->name;
        $email = $request->email;
        $password = $request->password;
        $password_confirmation = $request->password_confirmation;

        try{
            if($key == 'AbsensiMagang'){
                if($password == $password_confirmation){
                    if(User::where('email', $email)->count() > 0){
                        return response()->json([
                            'status' => 'Register Failed',
                            'message' => 'Email sudah terdaftar'
                        ], 401);
                    }
                    $user = User::create([
                        'name' => $name,
                        'email' => $email,
                        'password' => Hash::make($password),
                    ]);
                    event(new Registered($user));
                    Auth::login($user);
                    $request->user()->sendEmailVerificationNotification();
    
                    return response()->json([
                        'status' => 'Success',
                        'message' => 'Registrasi akun berhasil'
                    ], 200);
                }else{
                    return response()->json([
                        'status' => 'Error',
                        'message' => 'Konfirmasi password tidak sesuai'
                    ], 401);
                }
            }
        }catch(\Exception $exception){
            return response()->json([
                'status' => 'Error',
                'message' => 'System Error'
            ], 500);
        }
    }
}
