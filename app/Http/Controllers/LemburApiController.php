<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Lembur;
use Illuminate\Http\Request;

class LemburApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = $request->user;
        $key = $request->key;
        $id = $request->id;
        $tanggal_lembur = $request->tanggal_lembur;
        $dari_jam_lembur = $request->dari_jam_lembur;
        $hingga_jam_lembur = $request->hingga_jam_lembur;
        $keterangan = $request->keterangan;
        // dd($user);

        if($user == 'admin' && $key == 'AbsensiMagang' && $id !== null && $dari_jam_lembur !== null && $hingga_jam_lembur !== null && $keterangan !== null && $tanggal_lembur !== null){
            $name = User::where('id', $id)->get()->pluck('name')->toArray();
            $dari_jam_lembur = strtotime($request->dari_jam_lembur);
            $hingga_jam_lembur = strtotime($request->hingga_jam_lembur);
            $total_lembur = gmdate('H:i:s',($hingga_jam_lembur - $dari_jam_lembur));
            $angka_total_lembur = explode(':',$total_lembur);
            $angka_total_lembur = ($angka_total_lembur[0] * 60 + $angka_total_lembur[1])/60;
            $gaji_pokok = User::where('id', $id)->pluck('gaji_pokok')->toArray();
            $gaji_per_jam = $gaji_pokok[0] * (1 / 173);
            
            if($angka_total_lembur > 6){
                $nominal_lembur = $angka_total_lembur * $gaji_per_jam * 3;
            }else{
                $nominal_lembur = $angka_total_lembur * $gaji_per_jam * 2;
            }
            
            Lembur::create([
                'user_id' => $id,
                'name' => $name[0],
                'tanggal_lembur' => $tanggal_lembur,
                'dari_jam_lembur' => $request->dari_jam_lembur,
                'hingga_jam_lembur' => $request->hingga_jam_lembur,
                'total_jam_lembur' => $angka_total_lembur,
                'approval' => 'pending',
                'keterangan' => $keterangan,
                'nominal_lembur' => $nominal_lembur
            ]);
            
            return response()->json([
                'status' => 'Success',
                'message' => 'Berhasil melakukan submit pengajuan lembur'
            ], 200);
        }else{
            return response()->json([
                'status' => 'Error',
                'message' => 'Bad Auth'
            ], 401);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $user = 'admin';
        $key = 'AbsensiMagang';
        $id = $request->id;

        if($user == 'admin' && $key == 'AbsensiMagang' && $id != null){
            $data = Lembur::where('user_id', $id)->get();
            return response()->json([
                'status' => 'Success',
                'data' => $data
            ], 200);
        }else{
            return response()->json([
                'status' => 'Error',
                'message' => 'bad auth'
            ], 401);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
