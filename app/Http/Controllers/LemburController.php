<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Absen;
use App\Models\Lembur;
use Illuminate\Http\Request;

class LemburController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dari_jam_lembur = strtotime($request->dari_jam_lembur);
        $hingga_jam_lembur = strtotime($request->hingga_jam_lembur);
        $total_lembur = gmdate('H:i:s',($hingga_jam_lembur - $dari_jam_lembur));
        $angka_total_lembur = explode(':',$total_lembur);
        $angka_total_lembur = ($angka_total_lembur[0] * 60 + $angka_total_lembur[1])/60;
        $gaji_pokok = User::where('id', auth()->user()->id)->pluck('gaji_pokok')->toArray();
        $gaji_per_jam = $gaji_pokok[0] * (1 / 173);

        if($angka_total_lembur > 6){
            $nominal_lembur = $angka_total_lembur * $gaji_per_jam * 3;
        }else{
            $nominal_lembur = $angka_total_lembur * $gaji_per_jam * 2;
        }

        Lembur::create([
            'user_id' => $request->user_id,
            'name' => $request->name,
            'tanggal_lembur' => $request->tanggal_lembur,
            'dari_jam_lembur' => $request->dari_jam_lembur,
            'hingga_jam_lembur' => $request->hingga_jam_lembur,
            'total_jam_lembur' => $angka_total_lembur,
            'approval' => 'pending',
            'keterangan' => $request->keterangan,
            'nominal_lembur' => $nominal_lembur
        ]);

        return back()->with('berhasilSubmit', 'Pengajuan lembur berhasil terkirim');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Lembur  $lembur
     * @return \Illuminate\Http\Response
     */
    public function show(Lembur $lembur)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Lembur  $lembur
     * @return \Illuminate\Http\Response
     */
    public function edit(Lembur $lembur)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Lembur  $lembur
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lembur $lembur)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Lembur  $lembur
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lembur $lembur)
    {
        //
    }

    public function acceptRequestLembur($id)
    {
        Lembur::find($id)->update([
            'approval' => 'accept'
        ]);

        return back()->with('berhasilAccept', 'Permintaan lembur telah disetujui');
    }

    public function declineRequestLembur($id)
    {
        Lembur::find($id)->update([
            'approval' => 'decline'
        ]);

        return back()->with('berhasilDecline', 'Permintaan lembur tidak disetujui');
    }
}
