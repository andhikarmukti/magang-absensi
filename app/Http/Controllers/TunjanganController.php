<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Tunjangan;
use Illuminate\Http\Request;

class TunjanganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        // $gaji_pokok = User::where('id', auth()->user()->id)->get()->toArray();
        // $formula2 = $gaji_pokok[0][$request->formula2];
        // $formulaNominal = ($request->formula1/100) * $formula2;
        // dd($request->all());

        $request->validate([
            'persentase' => "numeric",
            'jenis_tunjangan' => "required",
            'pengali_berdasarkan' => 'required'
        ],
        [
            'persentase.numeric' => 'Masukkan hanya angka saja',
            'jenis_tunjangan.required' => 'Wajib diisi',
            'pengali_berdasarkan.required' => 'Wajib diisi'
        ]);

        Tunjangan::create([
            'jenis_tunjangan' => $request->jenis_tunjangan,
            'nominal' => $request->nominal,
            'persentase' => $request->persentase,
            'pengali_berdasarkan' => $request->pengali_berdasarkan

        ]);

        return back()->with('berhasilTambahDataTunjangan', 'Jenis tunjangan berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tunjangan  $tunjangan
     * @return \Illuminate\Http\Response
     */
    public function show(Tunjangan $tunjangan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tunjangan  $tunjangan
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Tunjangan::where('id', $id)->get()->toArray();

        return view('admin.pengaturan-tunjangan-edit',[
            'title' => 'Edit Pengaturan tunjangan',
            'data' => $data[0]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tunjangan  $tunjangan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tunjangan $tunjangan)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tunjangan  $tunjangan
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Tunjangan::destroy($id);
        return back()->with('delete', 'Jenis tunjangan berhasil dihapus');
    }
}
