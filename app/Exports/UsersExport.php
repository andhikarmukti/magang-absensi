<?php

namespace App\Exports;

use App\Models\User;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class UsersExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return User::all()->map(function ($user){
            return [
                'id' => $user->id,
                'name' => $user->name,
                'email' => $user->email,
             ];
        });
    }

    public function headings(): array
    {
        return ["id", "name", "email"];
    }
}
