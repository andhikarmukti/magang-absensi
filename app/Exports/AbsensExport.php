<?php

namespace App\Exports;

use App\Models\Absen;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class AbsensExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    /**
    * @return \Illuminate\Support\Collection
    */
    
    public function collection()
    {
        return DB::table('absens')->select([
                "name",
                'shift',
                'check_in',
                'check_out',
                'selisih_waktu',
                'keterangan',
                'created_at',
                'updated_at'
             ])->get();
    }

    public function headings(): array
    {
        return [
            "name",
            'shift',
            'check_in',
            'check_out',
            'selisih_waktu',
            'keterangan',
            'created_at',
            'updated_at'
        ];
    }
}