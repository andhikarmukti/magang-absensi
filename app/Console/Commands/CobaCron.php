<?php

namespace App\Console\Commands;

use App\Models\User;
use App\Models\Absen;
use Illuminate\Support\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CobaCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coba:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // return 0;
        //pengecekan libur nasional||
        $tanggalHoliday = DB::table('holidays')
        ->where('is_national_holiday', 1)
        ->whereDate('holiday_date', Carbon::today()->format('Y-m-d'))->get()->toArray();
        $jumlahUser = User::all();
        $user = $jumlahUser->toArray();
        $cekSudahAbsenHariIni = DB::table('absens')->whereDate('created_at', Carbon::today()->format('Y-m-d'))->get()->toArray();
        // dd(!$cekSudahAbsenHariIni);

        if ($tanggalHoliday) {
            if (!$cekSudahAbsenHariIni) {
                for ($i = 0; $i < $jumlahUser->count(); $i++) {
                    Absen::create([
                        'user_id' => $user[$i]['id'],
                        'name' => $user[$i]['name'],
                        'shift' => 'LIBUR NASIONAL',
                        'check_in' => 'LIBUR NASIONAL',
                        'check_out' => 'LIBUR NASIONAL',
                        'selisih_waktu' => null,
                        'check_in_image' => 'LIBUR NASIONAL',
                        'check_out_image' => 'LIBUR NASIONAL',
                        'keterangan' => 'LIBUR NASIONAL'
                    ]);
                }
            }
        }
    }
}
