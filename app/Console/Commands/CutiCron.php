<?php

namespace App\Console\Commands;

use App\Models\Cuti;
use App\Models\User;
use App\Models\Absen;
use Illuminate\Support\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class CutiCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cuti:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $cekTanggalCutiSamaDenganHariIni = Cuti::where('dari_tanggal', Carbon::today()->format('Y-m-d'))->where('approval', 'accept')->get()->pluck('dari_tanggal')->toArray();
        $cekBerapaBanyakIdYangCutiHariIni = Cuti::where('dari_tanggal', Carbon::today()->format('Y-m-d'))->where('approval', 'accept')->get()->pluck('user_id')->count();
        $cekIdSiapaYangCutiHariIni = Cuti::where('dari_tanggal', Carbon::today()->format('Y-m-d'))->where('approval', 'accept')->get()->pluck('user_id')->toArray();
        // dd($cekBerapaBanyakIdYangCutiHariIni);

        if ($cekTanggalCutiSamaDenganHariIni) {
            for ($i = 0; $i < $cekBerapaBanyakIdYangCutiHariIni; $i++) {
                $dataKaryawan = User::where('id', $cekIdSiapaYangCutiHariIni[$i])->get()->toArray();
                $cutiDariTanggal = Cuti::latest()->limit(1)->where('user_id', $dataKaryawan[0]['id'])
                    ->where('approval', 'accept')
                    ->pluck('dari_tanggal')->toArray();
                $cutiHinggaTanggal = Cuti::latest()->limit(1)->where('user_id', $dataKaryawan[0]['id'])
                    ->where('approval', 'accept')
                    ->pluck('hingga_tanggal')->toArray();
                $cekSubmitCutiHariIni = DB::table('absens')->whereDate('check_in', Carbon::now())->where('shift', 'cuti')->where('user_id', $dataKaryawan[0]['id'])->get()->toArray();
                // dd($cutiDariTanggal[0] == Carbon::today()->format('Y-m-d'));

                if (!$cekSubmitCutiHariIni) {
                    $selisihHariCuti = strtotime($cutiHinggaTanggal[0]) - strtotime($cutiDariTanggal[0]);
                    $totalHariCuti = abs($selisihHariCuti / (365 * 60 * 60 * 24) * 365) + 1;
                    // dd(date("Y-m-d", strtotime($cutiDariTanggal[0]) + 86400));
                    // dd($totalHariCuti);
                    for ($i = 0; $i < $totalHariCuti; $i++) {
                        $tanggalCuti = date("Y-m-d", strtotime($cutiDariTanggal[0]) + 86400 * $i);
                        Absen::create([
                            'name' => $dataKaryawan[0]['name'],
                            'user_id' => $dataKaryawan[0]['id'],
                            'shift' => 'cuti',
                            'check_in' => $tanggalCuti,
                            'check_out' => $tanggalCuti,
                            'jam_check_in' => 'cuti',
                            'jam_check_out' => 'cuti',
                            'selisih_waktu' => 'cuti',
                            'check_in_image' => 'cuti',
                            'check_out_image' => 'cuti',
                            'keterangan' => 'cuti'
                        ]);
                        
                    }

                    //pengurangan jatah cuti di table user
                    $jatah_cuti = User::where('id', $dataKaryawan[0]['id'])->pluck('jatah_cuti')->toArray();
                    $sisaCuti = $jatah_cuti[0] - $totalHariCuti;
                    User::where('id', $dataKaryawan[0]['id'])->update([
                        'jatah_cuti' => $sisaCuti
                    ]);
                }
            }
        }
    }
}
