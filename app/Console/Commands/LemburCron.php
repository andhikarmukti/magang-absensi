<?php

namespace App\Console\Commands;

use App\Models\Absen;
use Illuminate\Support\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class LemburCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lembur:crone';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tanggalLemburHariIni = DB::table('lemburs')->whereDate('tanggal_lembur', Carbon::today())->get()->pluck('tanggal_lembur')->toArray();
        $userIdYangLemburHariIni = DB::table('lemburs')->whereDate('tanggal_lembur', Carbon::today())->get()->pluck('user_id');
        $idAbsen = DB::table('absens')->whereDate('check_in', $tanggalLemburHariIni[0])->where('user_id', $userIdYangLemburHariIni)->get()->pluck('id')->toArray();
        // Absen::find($idAbsen[0])->update([
        //     'keterangan' => 'lembur'
        // ]);
    }
}
