<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAbsensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('absens', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('shift')->nullable();
            $table->string('check_in');
            $table->string('perubahan_check_in')->nullable();
            $table->string('check_out')->nullable();
            $table->string('selisih_waktu')->nullable();
            $table->string('jam_check_in')->nullable();
            $table->string('jam_check_out')->nullable();
            $table->string('check_in_image')->default('default.jpg');
            $table->string('check_out_image')->default('default.jpg');
            $table->string('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('absens');
    }
}
