<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        User::create([
            'name' => 'Andhika R Mukti',
            'email' => 'andhikarmukti@gmail.com',
            'password' => Hash::make('asdasdasd'),
            'role' => 'admin',
            'email_verified_at' => Carbon::now()
        ]);

        User::create([
            'name' => 'Tono',
            'email' => 'tono@gmail.com',
            'password' => Hash::make('asdasdasd'),
            'email_verified_at' => Carbon::now()
        ]);
    }
}
