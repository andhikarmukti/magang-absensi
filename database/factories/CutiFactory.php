<?php

namespace Database\Factories;

use App\Models\Cuti;
use Illuminate\Database\Eloquent\Factories\Factory;

class CutiFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Cuti::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
