<?php

namespace Database\Factories;

use App\Models\SlipGaji;
use Illuminate\Database\Eloquent\Factories\Factory;

class SlipGajiFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SlipGaji::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
