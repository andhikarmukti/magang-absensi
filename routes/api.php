<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CutiApiController;
use App\Http\Controllers\UserApiController;
use App\Http\Controllers\AbsenApiController;
use App\Http\Controllers\LoginApiController;
use App\Http\Controllers\ShiftApiController;
use App\Http\Controllers\LemburApiController;
use App\Http\Controllers\RegisterApiController;
use App\Http\Controllers\TunjanganApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

//Absensi
Route::get('/absenall', [AbsenApiController::class, 'allDataAbsen']);
Route::get('/absen', [AbsenApiController::class, 'absen']);
Route::post('/absen', [AbsenApiController::class, 'store']);
Route::delete('/absen/{id}', [AbsenApiController::class, 'destroy']);
Route::post('/perubahan-absensi', [AbsenApiController::class, 'perubahanAbsensi']);


Route::post('/cuti', [CutiApiController::class, 'store']);
Route::get('/jumlahMasukKerja', [AbsenApiController::class, 'jumlahMasukKerja']);

Route::post('/login', [LoginApiController::class, 'loginUser']);

Route::post('/register', [RegisterApiController::class, 'registerUser']);

//shift
Route::post('/shift', [ShiftApiController::class, 'store']);

//user
Route::get('user', [UserApiController::class, 'user']);

//tunjangan
Route::get('/tunjangan', [TunjanganApiController::class, 'tunjanganPerUser']);
Route::get('/nominalTunjangan', [TunjanganApiController::class, 'nominalTunjangan']);

//lembur
Route::post('/lembur', [LemburApiController::class, 'store']);
Route::get('/lembur', [LemburApiController::class, 'show']);