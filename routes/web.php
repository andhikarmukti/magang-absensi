<?php

use App\Models\Departemen;
use App\Models\TunjanganKaryawan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CutiController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AbsenController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\ShiftController;
use App\Http\Controllers\LemburController;
use App\Http\Controllers\HolidayController;
use App\Http\Controllers\TunjanganController;
use App\Http\Controllers\DepartemenController;
use App\Http\Controllers\SlipGajiController;
use App\Http\Controllers\TunjanganKaryawanController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

Auth::routes(['verify' => true]);

Route::get('/', [PagesController::class, 'home'])->middleware('auth', 'verified');
Route::post('/absen', [AbsenController::class, 'absen'])->middleware('auth', 'verified');

//request shift
Route::get('/request-shift', [PagesController::class, 'requestShift'])->middleware('auth', 'verified');
Route::post('/request-shift', [ShiftController::class, 'store'])->middleware('auth', 'verified');
Route::get('/list-request-shift', [PagesController::class, 'listRequestShift'])->middleware('auth');
Route::get('/list-request-shift/all', [PagesController::class, 'listAllRequestShift'])->middleware('auth');
Route::get('/accept-request/{id}', [ShiftController::class, 'acceptRequest'])->middleware('auth');
Route::get('/decline-request/{id}', [ShiftController::class, 'declineRequest'])->middleware('auth');

//request cuti
Route::get('/request-cuti', [PagesController::class, 'cuti'])->middleware('auth', 'verified');
Route::post('/request-cuti', [CutiController::class, 'store'])->middleware('auth', 'verified');
Route::get('/list-request-cuti', [PagesController::class, 'listRequestCuti'])->middleware('auth', 'verified');
Route::get('/list-request-cuti/all', [PagesController::class, 'listRequestCutiAll'])->middleware('auth', 'verified');
Route::get('/accept-request-cuti/{id}', [CutiController::class, 'acceptRequestCuti'])->middleware('auth', 'verified');
Route::get('/decline-request-cuti/{id}', [CutiController::class, 'declineRequestCuti'])->middleware('auth', 'verified');

//dashboard
Route::get('/dashboard', [PagesController::class, 'dashboard'])->middleware('admin', 'auth');
Route::get('/admin', [PagesController::class, 'admin'])->middleware('admin', 'auth');
Route::get('/history', [PagesController::class, 'history'])->middleware('admin', 'auth');
Route::get('/setting/pengaturan-shift', [PagesController::class, 'pengaturanShift'])->middleware('admin', 'auth');
Route::put('/pengaturan-shift', [UserController::class, 'pengaturanShiftUpdate'])->middleware('admin', 'auth');
Route::get('/setting/pengaturan-shift/edit/{id}', [PagesController::class, 'pengaturanShiftEdit'])->middleware('admin', 'auth');

//tunjangan
Route::get('/setting/pengaturan-tunjangan', [PagesController::class, 'pengaturanTunjangan'])->middleware('admin', 'auth');
Route::get('/setting/pengaturan-tunjangan-karyawan', [PagesController::class, 'pengaturanTunjanganKaryawan'])->middleware('admin', 'auth');
Route::post('/pengaturan-tunjangan-karyawan', [TunjanganKaryawanController::class, 'store'])->middleware('admin', 'auth');
Route::delete('/pengaturan-tunjangan-karyawan/{id}', [TunjanganKaryawanController::class, 'destroy'])->middleware('admin', 'auth');
Route::post('/pengaturan-tunjangan', [TunjanganController::class, 'store'])->middleware('admin', 'auth');
Route::get('/pengaturan-tunjangan/edit/{id}', [TunjanganController::class, 'edit'])->middleware('admin', 'auth');
Route::get('/pengaturan-tunjangan/delete/{id}', [TunjanganController::class, 'destroy'])->middleware('admin', 'auth');
Route::get('/setting/pengaturan-tunjangan-berdasarkan-tunjangan', [PagesController::class, 'pengaturanTunjanganBerdasarkanTunjangan'])->middleware('admin', 'auth');

Route::get('/profile/{id}', [PagesController::class, 'profile'])->middleware('auth', 'verified');
Route::post('/profile', [UserController::class, 'profile'])->middleware('auth', 'verified');

//attendance list & koreksi kehadiran
Route::get('/attendance-list', [PagesController::class, 'attendanceList'])->middleware('auth', 'verified');
Route::get('/pengajuan-perubahan-absen/{id}', [PagesController::class, 'pengajuanPerubahanAbsen'])->middleware('auth', 'verified');
Route::post('/pengajuan-perubahan-absen', [AbsenController::class, 'editPengajuanPerubahanAbsen'])->middleware('auth', 'verified');
Route::get('/request-koreksi-kehadiran', [PagesController::class, 'requestKoreksiKehadiran'])->middleware('auth', 'verified');
Route::get('/accept-perubahan/{id}', [AbsenController::class, 'acceptPerubahan'])->middleware('auth', 'verified', 'admin');
Route::get('/decline-perubahan/{id}', [AbsenController::class, 'declinePerubahan'])->middleware('auth', 'verified', 'admin');
Route::get('/setting', [PagesController::class, 'setting'])->middleware('auth', 'verified', 'admin');

//lembur
Route::get('/request-lembur', [PagesController::class, 'requestLembur'])->middleware('auth', 'verified');
Route::get('/list-request-lembur', [PagesController::class, 'listRequestLembur'])->middleware('auth', 'verified');
Route::get('/list-all-request-lembur', [PagesController::class, 'listAllRequestLembur'])->middleware('auth', 'verified');
Route::post('/request-lembur', [LemburController::class, 'store'])->middleware('auth', 'verified');
Route::get('/accept-request-lembur/{id}', [LemburController::class, 'acceptRequestLembur'])->middleware('auth', 'verified');
Route::get('/decline-request-lembur/{id}', [LemburController::class, 'declineRequestLembur'])->middleware('auth', 'verified');

//Departemen
Route::get('/departemen', [DepartemenController::class, 'index'])->middleware('auth', 'verified');
Route::post('/departemen', [DepartemenController::class, 'store'])->middleware('auth', 'verified');
Route::get('/departemen/tambah-ke-karyawan', [DepartemenController::class, 'tambahKeKaryawan'])->middleware('auth', 'verified');
Route::post('/departemen/tambah-ke-karyawan/store', [DepartemenController::class, 'tambahKeKaryawanStore'])->middleware('auth', 'verified');

//api documentation
Route::get('/tes', [PagesController::class, 'tes']);
Route::get('doc-api', [PagesController::class, 'docApi']);

Route::get('isi-keterangan', [AbsenController::class, 'isiKeterangan']);

//insert holiday for this year
// Route::get('/holiday', [HolidayController::class, 'holiday']);
Route::get('/holiday', [PagesController::class, 'holiday']);
Route::post('/holiday', [HolidayController::class, 'update']);
Route::get('/holiday/edit/{id}', [PagesController::class, 'holidayEdit']);

//export
Route::get('user-export', [UserController::class, 'userExport'])->name('user-export');
Route::get('absen-export', [AbsenController::class, 'absenExport'])->name('absen-export');

//Slip gaji
Route::get('/hitung-gaji', [SlipGajiController::class, 'create'])->middleware('auth', 'admin', 'verified');
Route::post('/hitung-gaji', [SlipGajiController::class, 'store'])->middleware('auth', 'admin', 'verified');

require __DIR__ . '/auth.php';
